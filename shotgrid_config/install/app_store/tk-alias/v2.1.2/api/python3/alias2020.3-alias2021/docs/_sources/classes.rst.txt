Classes
####################

.. currentmodule:: alias_api

Alternative
****************

.. autoclass:: Alternative
    :show-inheritance:
    :members:
    :inherited-members:

Layer
****************

.. autoclass:: Layer
    :show-inheritance:
    :members:
    :inherited-members:

Menu
****************

.. autoclass:: Menu
    :show-inheritance:
    :members:
    :inherited-members:

MenuItem
****************

.. autoclass:: MenuItem
    :show-inheritance:
    :members:
    :inherited-members:

Reference
****************

.. autoclass:: Reference
    :show-inheritance:
    :members:
    :inherited-members:

Stage
****************

.. autoclass:: Stage
    :show-inheritance:
    :members:
    :inherited-members:

Variant
****************

.. autoclass:: Variant
    :show-inheritance:
    :members:
    :inherited-members:
