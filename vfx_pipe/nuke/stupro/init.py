import os
import json
from show_scripts.customWriteNode import customWriteNode
from show_scripts.createNewScript import kbCreateNewScript
from show_scripts.autosave import autosave
# New untested customReadNode
from show_scripts.customReadNode import customReadNode

#################################################################################################
PIPE_PIPELINEPATH = os.getenv('PIPE_PIPELINEPATH')

with open(os.path.join(PIPE_PIPELINEPATH, "PIPELINE_CONSTANTS.json"), "r") as read_file:
    PIPELINE_CONSTANTS = json.load(read_file)

SG_PROJECTNAME = PIPELINE_CONSTANTS['SG_PROJECTNAME']
#################################################################################################
print("INITPY: show init.py loaded")

#### CUSTOM KNOBS
tabknob = nuke.Tab_Knob('shotsetup', 'shot setup')
project = nuke.String_Knob('project', 'project')
project.setFlag(nuke.READ_ONLY)
shotname = nuke.String_Knob('shotcode', 'shotcode')
shotname.setFlag(nuke.READ_ONLY)
artist = nuke.String_Knob("artist", "artist")
artist.setFlag(nuke.READ_ONLY)
task = nuke.String_Knob('task', 'task')
task.setFlag(nuke.READ_ONLY)
version = nuke.Int_Knob('version', 'version')
version.setFlag(nuke.READ_ONLY)
scriptcontext = nuke.Enumeration_Knob('scriptcontext', 'scriptcontext', ['Shot', 'Asset'])
scriptcontext.setEnabled(False)
assetType = nuke.String_Knob('assetType', 'type')
# maybe change visibilty flags
rootNode = nuke.root()
rootNode.addKnob(tabknob)
rootNode.addKnob(project)
rootNode.addKnob(shotname)
rootNode.addKnob(artist)
rootNode.addKnob(task)
rootNode.addKnob(version)
rootNode.addKnob(scriptcontext)
rootNode.addKnob(assetType)
nuke.root().knob('project').setValue(SG_PROJECTNAME)

#### Format
nuke.addFormat("3840 2160 UHD_4K") # Old Strive: 2048 1080 AMIRA_2k_workres ; Old: 2048 1152 AMIRA_2k_workres
nuke.addFormat("3950 2228 CG_overscan") # nach Distortion verbessern Old CP3: 2085 1173 CG_overscan OLD Strive: 2083 1171 CG_overscan
nuke.addFormat("3840 2160 global_output") #Old: 2048 1152 global_output
nuke.knobDefault("Root.format", "AMIRA_2k_workres")
nuke.knobDefault("Root.fps", "24")

#### Color
nuke.root()['colorManagement'].setValue('OCIO')

# Nodes
nuke.addOnCreate(customWriteNode.customWriteNode, nodeClass='Write')  # CustomWrites
# warning: not yet tested
nuke.addOnCreate(customReadNode.customReadNode, nodeClass='Read') # Custom Reads

# Defaults
nuke.addOnUserCreate(lambda: nuke.thisNode()['first_frame'].setValue(nuke.frame()), nodeClass='FrameHold')
nuke.knobDefault("EXPTool.mode", "Stops")


#### CALLBACKS
from show_scripts import scriptFix
# nuke.addOnScriptLoad(scriptFix.runScriptFix_onLoad)
nuke.addOnScriptSave(scriptFix.runScriptFix_onSave)
nuke.addOnScriptSave(customWriteNode.getCurrentContext)
nuke.addOnScriptSave(kbCreateNewScript.updateReadNodes)
nuke.addOnScriptSave(customReadNode.getCurrentContext)
