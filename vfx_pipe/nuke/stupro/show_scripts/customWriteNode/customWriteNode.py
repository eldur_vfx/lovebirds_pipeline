# This script adds the custom Write functionality

import os
import nuke
import json
import re


#################################################################################################
PIPE_PIPELINEPATH = os.getenv('PIPE_PIPELINEPATH')

with open(os.path.join(PIPE_PIPELINEPATH, "PIPELINE_CONSTANTS.json"), "r") as read_file:
    PIPELINE_CONSTANTS = json.load(read_file)

PIPE_PROJECTPATH = os.getenv('PIPE_PROJECTPATH')
PIPE_SHOTPATH = os.getenv('PIPE_SHOTPATH')
PIPE_ASSETPATH = os.getenv('PIPE_ASSETPATH')
SG_PROJECTNAME = PIPELINE_CONSTANTS['SG_PROJECTNAME']
#################################################################################################

renderSubdir = {
    #"img-cg": r'_export\img-cg',
    "img-int": r'_export\img-int',
    "mov": r'_export\mov',
    "img-jpg": r'_export\img-jpg',
    "img-fin": r'_export\img-fin'
}


def getCurrentContext():
    script_path = nuke.root().knob('name').value()
    script_name = os.path.basename(script_path)
    parts = script_name.split('_')
    version = re.findall("(_v)(\d{3})", script_name)
    artist = re.findall("[a-z]{2}\d{3}", script_name)

    try:
        nuke.root().knob('artist').setValue(artist[0])
        nuke.root().knob('task').setValue(parts[2])
        nuke.root().knob('shotcode').setValue(parts[0] + "_" + parts[1])
        nuke.root().knob('version').setValue(int(version[0][1]))
    except:
        pass


def updateWriteNodes(nodes, rootNode):
    getCurrentContext()

    code = rootNode['shotcode'].getValue()
    task = rootNode['task'].value()
    scriptVersion = rootNode['version'].getValue()
    artist = rootNode['artist'].getValue()
    scriptcontext = rootNode['scriptcontext'].value()
    assetType = rootNode['assetType'].value()

    dirScript = nuke.root().name()
    dir_list = dirScript.split('/')
    dir_list = dir_list[:-3] #only to overfolder STEP
    root_dir = '/'.join(dir_list) + '/'

    baseDir = os.path.join(root_dir)#PIPE_SHOTPATH, code, "2D"
    if scriptcontext == "Asset":
        baseDir = os.path.join(PIPE_ASSETPATH, assetType, code)

    for n in nodes:
        renderComment = n['comment'].getValue()
        writeRenderType = n['rendertype'].value()

        path = os.path.join(baseDir, renderSubdir[writeRenderType])

        if writeRenderType == "choose type":
            # does nothing because it's set to choose
            nuke.message('Please set a rendertype from the dropdown menu, THEN hit apply.')
            pass

        elif writeRenderType == "mov":

            n['file_type'].setValue('mov')
            n['colorspace'].setValue('sRGB Display: 2.2 Gamma - Rec.709')  # REPLACE!
            #n['mov_prores_codec_profile'].setValue('ProRes 4:2:2 10-bit')
            n['create_directories'].setValue(True)

            if renderComment == "":
                folder = '%s_%s_v%03d_%s' % (code, task, scriptVersion, artist)
            else:
                folder = '%s_%s_%s_v%03d_%s' % (code, task, renderComment, scriptVersion, artist)
            filename = '{}.mov'.format(folder)
            n['file'].setValue(os.path.join(path, filename).replace('\\', '/'))

       # elif writeRenderType == "img-cg":

        #    n['file_type'].setValue('exr')
        #    n['colorspace'].setValue('ACES - ACEScg')  # REPLACE!
        #    n['datatype'].setValue('16 bit half')  # 16bit float
        #    n['compression'].setValue('Zip (1 scanline)')
        #    n['metadata'].setValue('all metadata except input/*')
        #    n['autocrop'].setValue(False)
        #    n['create_directories'].setValue(True)

        #    if renderComment == "":
        #        folder = '%s_%s_v%03d_%s' % (code, task, scriptVersion, artist)
        #    else:
        #        folder = '%s_%s_%s_v%03d_%s' % (code, task, renderComment, scriptVersion, artist)
        #    filename = '{}.####.exr'.format(folder)
        #    n['file'].setValue(os.path.join(path, folder, filename).replace('\\', '/'))

        elif writeRenderType == "img-int":
            n['channels'].setValue('rgba')
            n['file_type'].setValue('exr')
            n['colorspace'].setValue('ACES - ACEScg')  # REPLACE!  ACES - ACEScg
            n['datatype'].setValue('16 bit half')  # 16bit float
            n['compression'].setValue('Zip (1 scanline)')
            n['metadata'].setValue('all metadata except input/*')
            n['autocrop'].setValue(False)
            n['create_directories'].setValue(True)

            if renderComment == "":
                folder = '%s_%s_v%03d_%s' % (code, task, scriptVersion, artist)
            else:
                folder = '%s_%s_%s_v%03d_%s' % (code, task, renderComment, scriptVersion, artist)
            filename = '{}.####.exr'.format(folder)
            n['file'].setValue(os.path.join(path, folder, filename).replace('\\', '/'))

        elif writeRenderType == "img-jpg":
            n['channels'].setValue('rgb')
            n['file_type'].setValue('jpeg')
            n['_jpeg_quality'].setValue(0.7)
            n['_jpeg_sub_sampling'].setValue('4:2:2')
            n['colorspace'].setValue('sRGB Display: 2.2 Gamma - Rec.709')  # REPLACE! Output - Rec.709
            n['create_directories'].setValue(True)

            if renderComment == "":
                folder = '%s_%s_v%03d_%s' % (code, task, scriptVersion, artist)
            else:
                folder = '%s_%s_%s_v%03d_%s' % (code, task, renderComment, scriptVersion, artist)
            filename = '{}.####.jpg'.format(folder)
            n['file'].setValue(os.path.join(path, folder, filename).replace('\\', '/'))

        elif writeRenderType == "img-fin":
            n['channels'].setValue('all')
            n['file_type'].setValue('exr')
            n['colorspace'].setValue('ACES - ACEScg')  # REPLACE! ACES - ACEScg
            n['datatype'].setValue('16 bit half')  # 16bit float
            n['compression'].setValue('Zip (1 scanline)')
            n['metadata'].setValue('all metadata except input/*')
            n['autocrop'].setValue(False)
            n['create_directories'].setValue(True)

            folder = '%s_%s_v%03d_%s' % (code, task, scriptVersion, artist)
            print(scriptVersion)
            filename = '{}.####.exr'.format(folder)
            n['file'].setValue(os.path.join(path, folder, filename).replace('\\', '/'))


def applyWrite():
    print("apply")
    n = nuke.thisNode()
    if re.search('[^A-Za-z0-9]', n['comment'].value()) is None:
        if n['rendertype'].value() != 'choose type':
            updateWriteNodes([n], nuke.root())
    else:
        nuke.message(
            'Write: ' + n.name() + ' comment contains special characters, please only use letters!')


def customWriteNode():  # adds pipeline functionality to write nodes, such as selectable rentertypes, filename comments
    node = nuke.thisNode()
    try:
        node['apply']
    except NameError:
        tabKnob = nuke.Tab_Knob('customTab', "VFX Stupro")#"CUSTOM"
        enumKnob = nuke.Enumeration_Knob('rendertype', 'rendertype',
                                         ['choose type', 'mov',  'img-int', 'img-jpg', "img-fin"])#'img-cg'
        # checkComment = nuke.Boolean_Knob('Test', )
        comment = nuke.String_Knob('comment', 'filename comment')
        applyButton = nuke.PyScript_Knob('apply', 'apply', "customWriteNode.applyWrite()")
        applyButton.setFlag(0x1000)

        node.addKnob(tabKnob)
        node.addKnob(enumKnob)
        node.addKnob(comment)
        node.addKnob(applyButton)

        node.knob('knobChanged').setValue(
            "n = nuke.thisNode()\nk = nuke.thisKnob()\nif k.name() == 'rendertype': \n    if k.value() in ["
            "'img-fin']:\n        n['comment'].setEnabled(False)\n    else:\n        n['comment'].setEnabled( "
            "True)\n\n")
