import os
import nuke
import re
import json

PIPE_SHOTPATH = os.getenv('PIPE_SHOTPATH')
PIPE_PIPELINEPATH = os.getenv('PIPE_PIPELINEPATH')
with open(os.path.join(PIPE_PIPELINEPATH, "PIPELINE_CONSTANTS.json"), "r") as read_file:
    PIPELINE_CONSTANTS = json.load(read_file)
SG_PROJECTNAME = PIPELINE_CONSTANTS['SG_PROJECTNAME']


def createNewCompScript():
    nuke.nodePaste(
        os.path.join(PIPE_PIPELINEPATH, 'nuke', 'stupro', 'Templates', 'TEMP_0000_master_comp_v002_ov004.nk'))
    updateReadNodes()


def createNewPrepScript():
    nuke.nodePaste(
        os.path.join(PIPE_PIPELINEPATH, 'nuke', 'stupro', 'Templates', 'TEMP_0000_master_prep_v001_ov004.nk'))
    updateReadNodes()


def createNewBasicScript():
    nuke.nodePaste(
        os.path.join(PIPE_PIPELINEPATH, 'nuke', 'stupro', 'Templates', 'TEMP_0000_master_basic_v001_dr069.nk'))
    updateReadNodes()


def updateReadNodes():
    print('UPDATE READ NODES!!!!!!!!!!!!!!!!!!')
    rootNode = nuke.root()
    code = rootNode['shotcode'].getValue()

    if code != '':
        seq_types = ['mp', 'cg', 'ep', 'prp']  # main plate, (clean plate), cg plate, lighting ref, prep plate
        for seqType in seq_types:

            # CG Plates
            if seqType is 'cg':
                cg_path = os.path.join(PIPE_SHOTPATH, code, '3D', 'anim', '_export', 'img-fin')
                max_version = 0
                cg_plate_folder = ""
                if os.path.exists(cg_path):
                    if os.listdir(cg_path):
                        for cg_version_folder in os.listdir(cg_path):
                            parts = cg_version_folder.split('_')
                            if len(parts) == 5:
                                version = int(parts[-2][1:4])
                                if version > max_version:
                                    max_version = version
                                    cg_plate_folder = cg_version_folder
                        if max_version != 0:
                            cg_seq_name = cg_plate_folder + '.####.exr'
                            cg_plate_path = os.path.join(cg_path, cg_plate_folder)
                            min = 10000
                            max = 0
                            if os.path.exists(cg_plate_path):
                                for img in os.listdir(cg_plate_path):
                                    if len(img.split('.')) > 2:
                                        frame = int(img.split('.')[1])
                                        if frame < min:
                                            min = frame

                                        if frame > max:
                                            max = frame

                                file_path = os.path.join(cg_plate_path, cg_seq_name)
                                file_path = file_path.replace('\\', '/')
                                n = nuke.toNode(seqType.upper())
                                if n:
                                    n.knob('file').setValue(file_path)
                                    n["first"].setValue(int(min))
                                    n["last"].setValue(int(max))

            # Preparation/Paintout Plates
            elif seqType is 'prp':
                prp_path = os.path.join(PIPE_SHOTPATH, code, '2D', 'prp', '_export', 'img-fin')
                max_version = 0
                plate_folder = ""
                if os.path.exists(prp_path):
                    if os.listdir(prp_path):
                        for version_folder in os.listdir(prp_path):
                            parts = version_folder.split('_')
                            if len(parts) == 5:
                                if parts[2] == seqType:
                                    version = int(parts[-2][1:4])
                                    if version > max_version:
                                        max_version = version
                                        plate_folder = version_folder
                        if max_version != 0:
                            seq_name = plate_folder + '.####.exr'
                            plate_path = os.path.join(prp_path, plate_folder)
                            min = 10000
                            max = 0
                            if os.path.exists(plate_path):
                                for img in os.listdir(plate_path):
                                    if len(img.split('.')) > 2:
                                        frame = int(img.split('.')[1])
                                        if frame < min:
                                            min = frame

                                        if frame > max:
                                            max = frame

                                file_path = os.path.join(plate_path, seq_name)
                                file_path = file_path.replace('\\', '/')
                                n = nuke.toNode(seqType.upper())
                                if n:
                                    n.knob('file').setValue(file_path)
                                    n["first"].setValue(int(min))
                                    n["last"].setValue(int(max))

            # Lighting Ref Plate
            elif seqType is 'ep':
                seq_name = code + '_lightRef.####.exr'
                epLight_path = os.path.join(PIPE_SHOTPATH, code, 'source', seqType, 'LightingRef')
                min = 10000
                max = 0
                if os.path.exists(epLight_path):
                    for img in os.listdir(epLight_path):
                        frame = int(img.split('.')[1])
                        if frame < min:
                            min = frame
                            print(min)
                        if frame > max:
                            max = frame

                    file_path = os.path.join(epLight_path, seq_name)
                    file_path = file_path.replace('\\', '/')
                    n = nuke.toNode(seqType.upper())
                    if n:
                        n.knob('file').setValue(file_path)
                        n["first"].setValue(int(min))
                        n["last"].setValue(int(max))

            # CleanPlate and MainPlate in one
            else:
                seq_name = code + '_' + seqType + '.####.exr'
                plate_path = os.path.join(PIPE_SHOTPATH, code, 'source', seqType)
                min = 10000
                max = 0
                if os.path.exists(plate_path):
                    for img in os.listdir(plate_path):
                        if img.split('.')[1] != 'x': #workaround, to allow a Buffercompressionfile from 3dEqualizer to be in the same folder as the mainplate
                            frame = int(img.split('.')[1])
                            if frame < min:
                                min = frame
                            if frame > max:
                                max = frame
                    setGlobalRange(min, max)
                    file_path = os.path.join(plate_path, seq_name)
                    file_path = file_path.replace('\\', '/')
                    n = nuke.toNode(seqType.upper())
                    if n:
                        n.knob('file').setValue(file_path)
                        n["first"].setValue(int(min))
                        n["last"].setValue(int(max))


def setGlobalRange(frameIn, frameOut):
    print("[deactivated] SET GLOBAL RANGE: {}-{}".format(frameIn, frameOut))
    # Disabled, values are garbage!!
    #nuke.knob("root.first_frame", str(frameIn))
    #nuke.knob("root.last_frame", str(frameOut))
