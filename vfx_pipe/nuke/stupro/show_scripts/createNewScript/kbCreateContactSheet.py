import os
import nuke
import re
import json
import configparser

""" 
This Script creates custom Contact Sheet. The file 'shotliste.ini' represents the underlying configurations.
Per Section there will be one Contact Sheet.
Have a look on the global variables and the comp_path if they match with you file path system.

CREATE extra Shot for Contact Sheet
CHANGE shotlist.ini
"""

PIPE_SHOTPATH = os.getenv('PIPE_SHOTPATH')
PIPE_PIPELINEPATH = os.getenv('PIPE_PIPELINEPATH')
with open(os.path.join(PIPE_PIPELINEPATH, "PIPELINE_CONSTANTS.json"), "r") as read_file:
    PIPELINE_CONSTANTS = json.load(read_file)
SG_PROJECTNAME = PIPELINE_CONSTANTS['SG_PROJECTNAME'].encode('utf-8')

SHOTLIST = os.path.join(PIPE_PIPELINEPATH, "nuke", 'stupro', "createNewScript", "shotliste.ini")
TASK_TO_LOOK_FOR = "cmp"


def createContactSheet():
    config = configparser.ConfigParser()
    config.read(SHOTLIST)
    for section in config.sections():
        sheet = nuke.nodes.ContactSheet(name=section)
        i = 0
        for (each_key, each_val) in config.items(section):
            read = nuke.nodes.Read(name=each_val)
            sheet.setInput(i, read)
            i += 1
    updateContactSheetExr()


def updateContactSheetExr():
    for node in nuke.allNodes(recurseGroups=True):
        if node.Class() == 'Read':
            code = node.fullName()
            comp_path = os.path.join(PIPE_SHOTPATH, code, '2D', 'comp', '_export', 'img-fin')
            max_version = -1
            plate_folder = ""
            if os.path.exists(comp_path):
                for folder in os.listdir(comp_path):
                    parts = folder.split('_')
                    if len(parts) > 4:
                        print(parts[2])
                        if parts[2] == TASK_TO_LOOK_FOR:
                            version = int(parts[-2][1:4])
                            if version > max_version:
                                max_version = version
                                plate_folder = folder
                if max_version != -1:
                    seq_name = plate_folder + '.####.exr'
                    plate_path = os.path.join(comp_path, plate_folder)
                    min = 10000
                    max = 0
                    if os.path.exists(plate_path):
                        for img in os.listdir(plate_path):
                            if len(img.split('.')) > 2:
                                frame = int(img.split('.')[1])
                                if frame < min:
                                    min = frame
                                    print(min)
                                if frame > max:
                                    max = frame

                        file_path = os.path.join(plate_path, seq_name)
                        file_path = file_path.replace('\\', '/')

                        node.knob('file').setValue(file_path)
                        node["first"].setValue(int(min))
                        node["last"].setValue(int(max))


def updateContactSheetJpg():
    for node in nuke.allNodes(recurseGroups=True):
        if node.Class() == 'Read':
            code = node.fullName()
            comp_path = os.path.join(PIPE_SHOTPATH, code, '2D', 'comp', '_export', 'img-jpg')
            max_version = -1
            plate_folder = ""
            if os.path.exists(comp_path):
                for folder in os.listdir(comp_path):
                    parts = folder.split('_')
                    if len(parts) > 4:
                        print(parts[2])
                        if parts[2] == TASK_TO_LOOK_FOR:
                            version = int(parts[-2][1:4])
                            if version > max_version:
                                max_version = version
                                plate_folder = folder
                if max_version != -1:
                    seq_name = plate_folder + '.####.jpg'
                    plate_path = os.path.join(comp_path, plate_folder)
                    min = 10000
                    max = 0
                    if os.path.exists(plate_path):
                        for img in os.listdir(plate_path):
                            if len(img.split('.')) > 2:
                                frame = int(img.split('.')[1])
                                if frame < min:
                                    min = frame
                                    print(min)
                                if frame > max:
                                    max = frame

                        file_path = os.path.join(plate_path, seq_name)
                        file_path = file_path.replace('\\', '/')

                        node.knob('file').setValue(file_path)
                        node["first"].setValue(int(min))
                        node["last"].setValue(int(max))