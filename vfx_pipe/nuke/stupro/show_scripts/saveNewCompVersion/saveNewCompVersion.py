import nuke
import nukescripts
import re
import os

def saveNewCompVersion(): #taken but modified from nukeescripts.script.script_version_up()
	root_name = nuke.root().name()
	setupVersionKnob = nuke.root()['version']
	#test if following naming convention via regex

	#test different user (script ownwer vs current user)
	#PANEL "You are a different artist than the one who currently owns this script!\nWould to like to increment with your own artist id or instead as the current artist?"
	
	try: 
		versionHelper = nukescripts.VersionHelper(root_name) 
		if versionHelper._currentV != setupVersionKnob.getValue(): 	#catch and fix inconsitencies between version knob and script version
			setupVersionKnob.setValue(versionHelper._currentV)
			nuke.scriptSave()
	except ValueError as e: 
		nuke.message("Unable to save new comp version:\n%s" % str(e)) 
		return 
	
	newFileName = versionHelper.nextVersionString() 
	newVersion = versionHelper.nextVersion() 

	# If the next version number already exists we need to ask the user how to proceed 
	newVersionExists = os.path.exists( newFileName ) 
	if newVersionExists: 
		versionDialog = nukescripts.VersionConflictDialog(versionHelper) 
		cancelVersionUp = not versionDialog.showDialog() 
		if cancelVersionUp: #conflict dialog canceled by user
			return 
		else: #conflict diaload to choose between override if identical version does exist or save as max version
			newFileName = versionDialog.getNewFilePath() 
			newVersion = versionDialog.getNewVersionNumber()

	#Make the new directory if needed 
	dirName = os.path.dirname( newFileName ) 
	if not os.path.exists( dirName ): 
		os.makedirs( dirName ) 
	
	#update version knob and write nodes
	setupVersionKnob.setValue(newVersion)
	for n in nuke.allNodes("Write"): #update write nodes
		n['apply'].execute()
	
	#Save the script and add to the bin 
	nuke.scriptSaveAs(newFileName) 
	if nuke.env['studio']: 
		from hiero.ui.nuke_bridge.nukestudio import addNewScriptVersionToBin 
		addNewScriptVersionToBin(root_name, newFileName) 

