# add KeenTools directory to python path to be able to import
# python files from KeenTools directory
import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__)))


from version_check import check_nuke_version_and_os


nuke.pluginAddPath('./icons')
if check_nuke_version_and_os('11.2', 'WIN', print_error_message=True):
    nuke.pluginAddPath('./plugin_libs')

    # optional environment variable that can be set to specify the path to the data directory
    # should be absolute or relative to the KeenTools library file (.so, .dll or .dylib)
    # os.environ["KEENTOOLS_DATA_PATH"] = "../data"

    # preload KeenTools so all the nodes will be available
    nuke.load('KeenTools')
else:
    print('loading KeenTools for Nuke11.2 WIN skipped')
