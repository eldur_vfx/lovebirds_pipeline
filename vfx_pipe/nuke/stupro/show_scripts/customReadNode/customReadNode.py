# This script adds the custom read functionality

import os
import nuke
import json
import re
import posixpath

#################################################################################################
PIPE_PIPELINEPATH = os.getenv('PIPE_PIPELINEPATH')

with open(os.path.join(PIPE_PIPELINEPATH, "PIPELINE_CONSTANTS.json"), "r") as read_file:
    PIPELINE_CONSTANTS = json.load(read_file)

PIPE_PROJECTPATH = os.getenv('PIPE_PROJECTPATH')
PIPE_SHOTPATH = os.getenv('PIPE_SHOTPATH')
PIPE_ASSETPATH = os.getenv('PIPE_ASSETPATH')
SG_PROJECTNAME = PIPELINE_CONSTANTS['SG_PROJECTNAME']
#################################################################################################

renderSubdir = {
    "img-cg": r'_export/img-cg',
    "img-int": r'_export/img-int',
    "img-jpg": r'_export/img-jpg',
    "img-fin": r'_export/img-fin'
}

taskSubdir = {
    "main plate": r'source/mp',
    "clean plate": r'source/cp',
    "greyball": r'source/ep/Greyball',
    "color checker": r'source/ep/ColorChecker',
    "comp": r'2D/cmp',
    "prep": r'2D/prp',
    "render": r'3D/rls',
    "playblast": r'3D/anm'
}


def getCurrentContext():
    script_path = nuke.root().knob('name').value()
    script_name = os.path.basename(script_path)
    parts = script_name.split('_')
    version = re.findall("(_v)(\d{3})", script_name)
    artist = re.findall("[a-z]{2}\d{3}", script_name)

    try:
        nuke.root().knob('artist').setValue(artist[0])
        nuke.root().knob('task').setValue(parts[2])
        nuke.root().knob('shotcode').setValue(parts[0] + "_" + parts[1])
        nuke.root().knob('version').setValue(int(version[0][1]))
    except:
        pass


def updateReadNodes(nodes, rootNode):
    getCurrentContext()

    code = rootNode['shotcode'].getValue()
    task = rootNode['task'].value()
    scriptVersion = rootNode['version'].getValue()
    artist = rootNode['artist'].getValue()
    scriptcontext = rootNode['scriptcontext'].value()
    assetType = rootNode['assetType'].value()

    # Only update when code is set
    if code == '' or code == None:
        return

    dirScript = nuke.root().name()
    dir_list = dirScript.split('/')
    dir_list = dir_list[:-5] #only to overfolder STEP
    root_dir = '/'.join(dir_list) + '/'

    baseDir = posixpath.join(root_dir) #PIPE_SHOTPATH

    for n in nodes:
        sequences = {}

        # Build base path where to look for sequences
        curTaskIndex = int(n['fromtask'].getValue())
        curTask = list(taskSubdir.keys())[curTaskIndex]
        curTaskPath = list(taskSubdir.values())[curTaskIndex]
        baseDir = posixpath.join(baseDir, curTaskPath)

        # Add render subdir for tasks
        if curTask in ['comp', 'prep', 'render', 'playblast']:
            curTypeIndex = int(n['fromtype'].getValue())
            curType = list(renderSubdir.keys())[curTypeIndex]
            baseDir = posixpath.join(baseDir, renderSubdir[curType])

            if curType in ['img-fin']:
                # No comments, just search for versions in folders
                sequences = addSequences(baseDir, inFolders=True, sequences=sequences)

            else:
                # Filter by comments, if no comment is entered display all, starting with sequences without comment (if any)
                curComment = n['comment'].getValue()
                listAll = True if curComment == '' else False
                sequences = addSequences(baseDir, inFolders=True, comment=curComment, showAllComments=listAll, sequences=sequences)

        else:
            # Search for versions in files (no subfolders)
            sequences = addSequences(baseDir, inFolders=False, sequences=sequences)
        
        seqList = []
        pathList = []
        for key in sequences:
            sShot, sTask, sComment, sVersion, sName, sRangeMin, sRangeMax = sequences[key]
            seqDescr = f"{sShot}_{sTask} v{sVersion}: {sRangeMin}-{sRangeMax}"
            if sName != '':
                seqDescr += ' ('
                if sComment != '':
                    seqDescr += f'{sComment} - '
                seqDescr += f'{sName})'
            seqList.append(seqDescr)
            pathList.append(key+f" {sRangeMin}-{sRangeMax}")
            
        n['seq_version'].setValues(seqList)
        n['pathlist'].setValues(pathList)
    

def addSequences(baseDir, inFolders, comment='', showAllComments=False, sequences={}):
    # Key = full path 
    # Value = (shot, task, comment, version, name, range_min, range_max)
    if inFolders:
        try:
            # Look for folders containing files and call function recursivly
            files = os.listdir(baseDir)
            for dir in files:
                newDir = posixpath.join(baseDir, dir)
                if os.path.isdir(newDir):
                    sequences = addSequences(newDir, False, comment=comment, showAllComments=showAllComments, sequences=sequences)
        except:
            pass

    else:
        # Look for files in current folder
        files = os.listdir(baseDir)
        for filename in files:
            match = re.fullmatch('(([A-Za-z]+_[0-9]+)_([A-Za-z]+)(?:_([A-Za-z0-9\-]+))?_v([0-9]+)(?:_([a-z]+[0-9]+))?\.)([0-9]+)\.exr', filename)
            if match:
                match = match.groups()
                # Add only sequences the user was looking for
                foundComment = match[3]
                path = posixpath.join(baseDir, match[0]+'####.exr')
                if comment == '' and not foundComment:
                    sequences = _sequenceAddHelper(sequences, match, path)
                elif (comment != '' or showAllComments):
                    sequences = _sequenceAddHelper(sequences, match, path)

    return sequences
            
def _sequenceAddHelper(sequences, match, path):
    # Add to sequence dict
    if path in sequences:
        # Update min & max values
        curFrame = match[-1]
        if int(sequences[path][5]) > int(curFrame):
            sequences[path][5] = curFrame
        elif int(sequences[path][6]) < int(curFrame):
            sequences[path][6] = curFrame
    else:
        # Add new entry to dict: (shot, task, comment, version, name, range_min, range_max)
        sequences[path] = [match[1], match[2], match[3] if match[3] else '', match[4], match[5] if match[5] else '', match[-1], match[-1]]
    return sequences


def applySearch():
    print("search")

    n = nuke.thisNode()
    # make sure the comment field contains only allowed characters and a render type is selected
    if re.search('[^A-Za-z0-9]', n['comment'].value()) is None:
        updateReadNodes([n], nuke.root())
    else:
        nuke.message(
            'Read: ' + n.name() + ' comment contains special characters, please only use letters!')

def applyRead():
    print("apply")

    n = nuke.thisNode()
    # A version has to be selected
    if n['seq_version'].getValue() != '':
        # Get selected version and insert path to node
        selectionIndex = int(n['seq_version'].getValue())
        if selectionIndex >= 0:
            n['file'].fromUserText(n['pathlist'].values()[selectionIndex])


def customReadNode():  # adds pipeline functionality to read nodes, such as selectable rentertypes, filename comments
    node = nuke.thisNode()
    try:
        node['apply']
    except NameError:
        # Tab
        tabKnob = nuke.Tab_Knob('customTab', "VFX Stupro")
        taskKnob = nuke.Enumeration_Knob('fromtask', 'Task', list(taskSubdir.keys()))
        # Render Type list/enum
        typeKnob = nuke.Enumeration_Knob('fromtype', 'Render Type', list(renderSubdir.keys()))
        # checkComment = nuke.Boolean_Knob('Test', )
        # Comment string
        comment = nuke.String_Knob('comment', 'File Comment')
        # Search Button
        searchButton = nuke.PyScript_Knob('search', 'Search', "customReadNode.applySearch()")
        searchButton.setFlag(0x1000)
        # Version list/enum
        seqVersionKnob = nuke.Enumeration_Knob('seq_version', 'Sequence Version', [])
        # Apply Button
        applyButton = nuke.PyScript_Knob('apply', 'Apply', "customReadNode.applyRead()")
        applyButton.setFlag(0x1000)

        # Path list
        pathList = nuke.Enumeration_Knob('pathlist', 'pathlist', [])
        pathList.setFlag(0x00000400) # hidden

        node.addKnob(tabKnob)
        node.addKnob(taskKnob)
        node.addKnob(typeKnob)
        node.addKnob(comment)
        node.addKnob(searchButton)
        node.addKnob(seqVersionKnob)
        node.addKnob(applyButton)
        node.addKnob(pathList)

        node.knob('knobChanged').setValue("""
n = nuke.thisNode()
k = nuke.thisKnob()
if k.name() == 'fromtask' or k.name() == 'fromtype':
    if n['fromtask'].value() in ['main plate', 'clean plate', 'greyball', 'color checker']:
        n['fromtype'].setEnabled(False)
        n['comment'].setEnabled(False)
    elif n['fromtype'].value() in ['img-fin']:
        n['fromtype'].setEnabled(True)
        n['comment'].setEnabled(False)
    else:
        n['fromtype'].setEnabled(True)
        n['comment'].setEnabled(True)
""")
        node.knob('knobChanged').execute()
