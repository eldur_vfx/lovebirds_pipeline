import maya.cmds as mc
import maya.mel as mm

class ui_OVDB():

	def initialize(self):

		# delete existing menu
		l = mc.lsUI(typ="menu") or []
		for m in l:
			if mc.menu(m, q=True, ex=True):
				if mc.menu(m, q=True, l=True) == "SOuP/OVDB":
					mc.deleteUI(m)

		# indirectly check if plugins are loaded
		try: soup = mc.pluginInfo("SOuP", q=True, dn=True)
		except: soup = None
		try: ovdb = mc.pluginInfo("BE_OpenVDB", q=True, dn=True)
		except: ovdb = None

		# if both plugins are unloaded - quit
		if soup == None and ovdb == None: return

		# monkey-patch an obsolete SOuP callback
		if soup == None:
			try: mm.eval('python("class ui_SOuP():\\n\\tdef initialize(self): pass")')
			except: pass

		# create new menu
		menu = mm.eval('menu -p $gMainWindow -to true -l "SOuP/OVDB"')
		if soup != None:
			# if SOuP is loaded - this call will do the right thing
			try: ui_SOuP().create(menu, soup)
			except: pass
		if ovdb != None: self.create(menu, ovdb)
		mc.menuItem(d=True, p=menu)
		mc.menuItem(l="print host id", c=self.printHostId, p=menu)

	def create(self, menu, nodes):

		ovdb = mc.menuItem(l="OVDB nodes", i="OpenVdbNode.xpm", sm=True, to=True, p=menu)
#		for n in sorted(nodes): mc.menuItem(l=n, c="mc.createNode('"+n+"')", p=ovdb)
		s = """menuItem  -p """+ovdb+""" -l "Advect" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem1;
menuItem -l "BE_VDBAdvect" -image "OpenVdbNode.xpm" -command "BE_VDBAdvect";
menuItem -l "BE_VDBAdvectSDF" -image "OpenVdbNode.xpm" -command "BE_VDBAdvectSDF";
menuItem -l "BE_VDBAdvectPoints" -image "OpenVdbNode.xpm" -command "BE_VDBAdvectPoints";
menuItem -l "BE_VDBMorphSDF" -image "OpenVdbNode.xpm" -command "BE_VDBMorphSDF";
menuItem -l "BE_VDBGasSolver" -image "OpenVdbNode.xpm" -command "BE_VDBGasSolver";

menuItem  -p """+ovdb+""" -l "Convert" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem2;
menuItem -l "BE_VDBConvertVDB" -image "OpenVdbNode.xpm" -command "BE_VDBConvertVDB";
menuItem -l "BE_VDBFromMask" -image "OpenVdbNode.xpm" -command "BE_VDBFromMask";
menuItem -l "BE_VDBFromPolygons" -image "OpenVdbNode.xpm" -command "BE_VDBFromPolygons";
menuItem -l "BE_VDBFromParticles" -image "OpenVdbNode.xpm" -command "BE_VDBFromParticles";
menuItem -l "BE_VDBFromMayaFluid" -image "OpenVdbNode.xpm" -command "BE_VDBFromMayaFluid";
menuItem -l "BE_VDBToMayaFluid" -image "OpenVdbNode.xpm" -command "BE_VDBToMayaFluid";
menuItem -l "BE_VDBTopologyMask" -image "OpenVdbNode.xpm" -command "BE_VDBTopologyMask";
menuItem -l "BE_VDBPointsConvert" -image "OpenVdbNode.xpm" -command "BE_VDBPointsConvert";
menuItem -l "BE_VDBPointsToArray" -image "OpenVdbNode.xpm" -command "BE_VDBPointsToArray";
menuItem -l "BE_VDBSource" -image "OpenVdbNode.xpm" -command "BE_VDBSource";

menuItem  -p """+ovdb+""" -l "Data" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem3;
menuItem -l "BE_VDBGetData" -image "OpenVdbNode.xpm" -command "BE_VDBGetData";
menuItem -l "BE_VDBSetData" -image "OpenVdbNode.xpm" -command "BE_VDBSetData";
menuItem -l "BE_VDBAttributeFromVDB" -image "OpenVdbNode.xpm" -command "BE_VDBAttributeFromVDB";

menuItem  -p """+ovdb+""" -l "Filter" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem4;
menuItem -l "BE_VDBFilter" -image "OpenVdbNode.xpm" -command "BE_VDBFilter";
menuItem -l "BE_VDBFilterSDF" -image "OpenVdbNode.xpm" -command "BE_VDBFilterSDF";

menuItem  -p """+ovdb+""" -l "I/O" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem5;
menuItem -l "BE_VDBRead" -image "OpenVdbNode.xpm" -command "BE_VDBRead";
menuItem -l "BE_VDBWrite" -image "OpenVdbNode.xpm" -command "BE_VDBWrite";

menuItem  -p """+ovdb+""" -l "Noise" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem6;
menuItem -l "BE_VDBNoise" -image "OpenVdbNode.xpm" -command "BE_VDBNoise";
menuItem -l "BE_VDBFlowNoise" -image "OpenVdbNode.xpm" -command "BE_VDBFlowNoise";

menuItem  -p """+ovdb+""" -l "Manipulate" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem7;
menuItem -l "BE_VDBDensify" -image "OpenVdbNode.xpm" -command "BE_VDBDensify";
menuItem -l "BE_VDBTransform" -image "OpenVdbNode.xpm" -command "BE_VDBTransform";
menuItem -l "BE_VDBResample" -image "OpenVdbNode.xpm" -command "BE_VDBResample";
menuItem -l "BE_VDBRemap" -image "OpenVdbNode.xpm" -command "BE_VDBRemap";
menuItem -l "BE_VDBClip" -image "OpenVdbNode.xpm" -command "BE_VDBClip";
menuItem -l "BE_VDBCombine" -image "OpenVdbNode.xpm" -command "BE_VDBCombine";
menuItem -l "BE_VDBActivate" -image "OpenVdbNode.xpm" -command "BE_VDBActivate";
menuItem -l "BE_VDBAnalysis" -image "OpenVdbNode.xpm" -command "BE_VDBAnalysis";
menuItem -l "BE_VDBPotentialFlow" -image "OpenVdbNode.xpm" -command "BE_VDBPotentialFlow";
menuItem -l "BE_VDBRemoveDivergence" -image "OpenVdbNode.xpm" -command "BE_VDBRemoveDivergence";
menuItem -l "BE_VDBRebuildLevelSet" -image "OpenVdbNode.xpm" -command "BE_VDBRebuildLevelSet";

menuItem  -p """+ovdb+""" -l "Vis/Render" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem8;
menuItem -l "BE_VDBVisualize" -image "OpenVdbNode.xpm" -command "BE_VDBVisualize";
menuItem -l "BE_VDBArnoldRender" -image "BE_VDBArnoldRender.png" -command "BE_VDBArnoldRender";
menuItem -l "BE_VDBPointsRender" -image "BE_VDBPointsRender.png" -command "BE_VDBPointsRender";
menuItem -l "BE_VDBVolumeTrail" -image "BE_VDBVolumeTrail.png" -command "BE_VDBVolumeTrail";
menuItem -l "BE_VDBPreview" -image "OpenVdbNode.xpm" -command "BE_VDBPreview";

menuItem  -p """+ovdb+""" -l "Vector" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem9;
menuItem -l "BE_VDBVectorMerge" -image "OpenVdbNode.xpm" -command "BE_VDBVectorMerge";
menuItem -l "BE_VDBVectorSplit" -image "OpenVdbNode.xpm" -command "BE_VDBVectorSplit";

menuItem  -p """+ovdb+""" -l "Utility" -image "OpenVdbNode.xpm" -tearOff false -subMenu true menuItem10;
menuItem -l "BE_VDBLOD" -image "OpenVdbNode.xpm" -command "BE_VDBLOD";
menuItem -l "BE_VDBRay" -image "OpenVdbNode.xpm" -command "BE_VDBRay";
menuItem -l "BE_VDBSwitch" -image "OpenVdbNode.xpm" -command "BE_VDBSwitch";
menuItem -l "BE_VDBCopy" -image "OpenVdbNode.xpm" -command "BE_VDBCopy";
menuItem -l "BE_VDBRename" -image "OpenVdbNode.xpm" -command "BE_VDBRename";
menuItem -l "BE_VDBFill" -image "OpenVdbNode.xpm" -command "BE_VDBFill";
menuItem -l "BE_VDBFracture" -image "OpenVdbNode.xpm" -command "BE_VDBFracture";
menuItem -l "BE_VDBScatter" -image "OpenVdbNode.xpm" -command "BE_VDBScatter";
menuItem -l "BE_VDBPointsDelete" -image "OpenVdbNode.xpm" -command "BE_VDBPointsDelete";


menuItem  -p """+ovdb+""" -l "Bifrost" -image "BE_ArrayNodes.bmp" -tearOff false -subMenu true menuItem11;
menuItem -l "BE_BifrostFileToArray" -image "BE_ArrayNodes.bmp" -command "BE_BifrostFileToArray";
menuItem -l "BE_BifrostFileToParticles" -image "BE_ArrayNodes.bmp" -command "BE_BifrostFileToParticles";
menuItem -l "BE_BifrostFileVoxelSampler" -image "BE_ArrayNodes.bmp" -command "BE_BifrostFileVoxelSampler";
menuItem -l "BE_BifrostFileToVDB" -image "BE_ArrayNodes.bmp" -command "BE_BifrostFileToVDB";
menuItem -l "BE_BifrostToArray" -image "BE_ArrayNodes.bmp" -command "BE_BifrostToArray";

menuItem  -p """+ovdb+""" -l "Math" -image "BE_ArrayNodes.bmp" -tearOff false -subMenu true menuItem12;
menuItem -l "BE_MathFlt" -image "BE_ArrayNodes.bmp" -command "BE_MathFlt";
menuItem -l "BE_MathVec" -image "BE_ArrayNodes.bmp" -command "BE_MathVec";
menuItem -l "BE_Fit" -image "BE_ArrayNodes.bmp" -command "BE_Fit";

menuItem  -p """+ovdb+""" -l "Vector" -image "BE_ArrayNodes.bmp" -tearOff false -subMenu true menuItem13;
menuItem -l "BE_CrossProduct" -image "BE_ArrayNodes.bmp" -command "BE_CrossProduct";
menuItem -l "BE_DotProduct" -image "BE_ArrayNodes.bmp" -command "BE_DotProduct";
menuItem -l "BE_Distance" -image "BE_ArrayNodes.bmp" -command "BE_Distance";
menuItem -l "BE_Length" -image "BE_ArrayNodes.bmp" -command "BE_Length";
menuItem -l "BE_Normalize" -image "BE_ArrayNodes.bmp" -command "BE_Normalize";

menuItem  -p """+ovdb+""" -l "Noise" -image "BE_ArrayNodes.bmp" -tearOff false -subMenu true menuItem14;
menuItem -l "BE_AntiAliasedFlowNoise" -image "BE_ArrayNodes.bmp" -command "BE_AntiAliasedFlowNoise";
menuItem -l "BE_TurbulenceNoise" -image "BE_ArrayNodes.bmp" -command "BE_TurbulenceNoise";
menuItem -l "BE_fbmNoise" -image "BE_ArrayNodes.bmp" -command "BE_fbmNoise";
menuItem -l "BE_WorleyNoise" -image "BE_ArrayNodes.bmp" -command "BE_WorleyNoise";

menuItem  -p """+ovdb+""" -l "Convert" -image "BE_ArrayNodes.bmp" -tearOff false -subMenu true menuItem15;
menuItem -l "BE_ConvertScalarArray" -image "BE_ArrayNodes.bmp" -command "BE_ConvertScalarArray";
menuItem -l "BE_FloatToVector" -image "BE_ArrayNodes.bmp" -command "BE_FloatToVector";
menuItem -l "BE_VectorToFloat" -image "BE_ArrayNodes.bmp" -command "BE_VectorToFloat";

menuItem  -p """+ovdb+""" -l "Utility" -image "BE_ArrayNodes.bmp" -tearOff false -subMenu true menuItem16;
menuItem -l "BE_DeletePointByValue" -image "BE_ArrayNodes.bmp" -command "BE_DeletePointByValue";
menuItem -l "BE_Ramp" -image "BE_ArrayNodes.bmp" -command "BE_Ramp";
menuItem -l "BE_Velocity" -image "BE_ArrayNodes.bmp" -command "BE_Velocity";
menuItem -l "BE_Switch" -image "BE_ArrayNodes.bmp" -command "BE_Switch";
menuItem -l "BE_GetPoints" -image "BE_ArrayNodes.bmp" -command "BE_GetPoints";
menuItem -l "BE_SetPoints" -image "BE_ArrayNodes.bmp" -command "BE_SetPoints";
menuItem -l "BE_SeExprPoint" -image "BE_ArrayNodes.bmp" -command "BE_SeExprPoint";
menuItem -l "InsertFractures" -command "InsertFracture";

menuItem  -p """+ovdb+""" -l "InsertFracture" -command "InsertFracture";

global proc BE_VDBRead(){createNode  BE_VDBRead;}
global proc BE_VDBWrite(){createNode  BE_VDBWrite;}
global proc BE_VDBCopy(){createNode  BE_VDBCopy;}
global proc BE_VDBSwitch(){createNode  BE_VDBSwitch;}
global proc BE_VDBActivate(){createNode  BE_VDBActivate;}
global proc BE_VDBAdvect(){createNode  BE_VDBAdvect;}
global proc BE_VDBAdvectSDF(){createNode  BE_VDBAdvectSDF;}
global proc BE_VDBAdvectPoints(){createNode  BE_VDBAdvectPoints;}
global proc BE_VDBAnalysis(){createNode  BE_VDBAnalysis;}
global proc BE_VDBClip(){createNode  BE_VDBClip;}
global proc BE_VDBCombine(){createNode  BE_VDBCombine;}
global proc BE_VDBConvertVDB(){createNode  BE_VDBConvertVDB;}
global proc BE_VDBFill(){createNode  BE_VDBFill;}
global proc BE_VDBFilter(){createNode  BE_VDBFilter;}
global proc BE_VDBFilterSDF(){createNode  BE_VDBFilterSDF;}
global proc BE_VDBNoise(){createNode  BE_VDBNoise;}
global proc BE_VDBFlowNoise(){createNode  BE_VDBFlowNoise;}
global proc BE_VDBFracture(){createNode  BE_VDBFracture;}
global proc BE_VDBPointsConvert(){createNode  BE_VDBPointsConvert;}
global proc BE_VDBPointsToArray(){createNode  BE_VDBPointsToArray;}
global proc BE_VDBFromMask(){createNode  BE_VDBFromMask;}
global proc BE_VDBFromMayaFluid(){createNode  BE_VDBFromMayaFluid;}
global proc BE_VDBFromParticles(){createNode  BE_VDBFromParticles;}
global proc BE_VDBFromPolygons(){createNode  BE_VDBFromPolygons;}
global proc BE_VDBSource(){createNode  BE_VDBSource;}

global proc BE_VDBToMayaFluid()
{
string $fluidNode = `createNode -n "fluidShape#" fluidShape`;
select $fluidNode;
string $obj[] = `ls -sl`;
string $parents[] = `listRelatives -fullPath -parent $obj[0]`;

string $VDBToMayaFluid = `createNode  BE_VDBToMayaFluid`;

connectAttr ($VDBToMayaFluid+".DensityOut") ($fluidNode+".inDensity");
connectAttr ($VDBToMayaFluid+".TemperatureOut") ($fluidNode+".inTemperature");
connectAttr ($VDBToMayaFluid+".VelocityOut") ($fluidNode+".inVelocity");
connectAttr ($VDBToMayaFluid+".ColorOut") ($fluidNode+".inColor");
connectAttr ($VDBToMayaFluid+".OffsetOut") ($fluidNode+".inOffset");
connectAttr ($VDBToMayaFluid+".ResolutionOut") ($fluidNode+".inResolution");
connectAttr ($VDBToMayaFluid+".TemperatureMethod") ($fluidNode+".temperatureMethod");
connectAttr ($VDBToMayaFluid+".VelocityMetod") ($fluidNode+".velocityMethod");
connectAttr ($VDBToMayaFluid+".ColorMethod") ($fluidNode+".colorMethod");
connectAttr ($VDBToMayaFluid+".PlayFromCashe") ($fluidNode+".playFromCache");

setAttr  ($fluidNode+".dimensionsW") 1;
setAttr  ($fluidNode+".dimensionsH") 1;
setAttr  ($fluidNode+".dimensionsD") 1;
setAttr  ($fluidNode+".boundaryDraw") 4;

connectAttr ($VDBToMayaFluid+".Scale") ($parents[0]+".scale");
connectAttr ($VDBToMayaFluid+".Translate") ($parents[0]+".translate");
connectAttr ($VDBToMayaFluid+".Rotate") ($parents[0]+".rotate");
}

global proc BE_VDBGetData(){createNode  BE_VDBGetData;}
global proc BE_VDBMorphSDF(){createNode  BE_VDBMorphSDF;}
global proc BE_VDBGasSolver(){createNode  BE_VDBGasSolver;}
global proc BE_VDBRay(){createNode  BE_VDBRay;}
global proc BE_VDBRename(){createNode  BE_VDBRename;}
global proc BE_VDBResample(){createNode  BE_VDBResample;}
global proc BE_VDBRemap(){createNode  BE_VDBRemap;}
global proc BE_VDBRebuildLevelSet(){createNode  BE_VDBRebuildLevelSet;}
global proc BE_VDBRemoveDivergence(){createNode  BE_VDBRemoveDivergence;}
global proc BE_VDBScatter(){createNode  BE_VDBScatter;}
global proc BE_VDBSetData(){createNode  BE_VDBSetData;}
global proc BE_VDBAttributeFromVDB(){createNode  BE_VDBAttributeFromVDB;}
global proc BE_VDBTopologyMask(){createNode  BE_VDBTopologyMask;}
global proc BE_VDBTransform(){createNode  BE_VDBTransform;}
global proc BE_VDBLOD(){createNode  BE_VDBLOD;}
global proc BE_VDBVectorMerge(){createNode  BE_VDBVectorMerge;}
global proc BE_VDBVectorSplit(){createNode  BE_VDBVectorSplit;}
global proc BE_VDBDensify(){createNode  BE_VDBDensify;}
global proc BE_VDBPointsDelete(){createNode  BE_VDBPointsDelete;}
global proc BE_VDBPotentialFlow(){createNode  BE_VDBPotentialFlow;}

global proc BE_VDBVisualize()
{
	string $visNode = `createNode -n "BE_VDBVisualizeShape#" BE_VDBVisualize`;
	string $VDBRead = `createNode  BE_VDBRead`;
	connectAttr ($VDBRead+".vdb") ($visNode+".input");
	select $VDBRead;
}

global proc BE_VDBArnoldRender(){BE_VDBArnoldRenderCreate;}
global proc BE_VDBPointsRender(){BE_VDBPointsRenderCreate;}
global proc BE_VDBVolumeTrail(){BE_VDBVolumeTrailCreate;}
global proc BE_VDBPreview(){BE_VDBPreviewCreate;}
global proc BE_BifrostFileToArray()    {createNode  BE_BifrostFileToArray;}
global proc BE_BifrostFileToVDB()    {createNode  BE_BifrostFileToVDB;}
global proc BE_BifrostFileVoxelSampler()    {createNode  BE_BifrostFileVoxelSampler;}
global proc BE_BifrostToArray()        {createNode  BE_BifrostToArray;}
global proc BE_MathFlt()               {createNode  BE_MathFlt;}
global proc BE_MathVec()               {createNode  BE_MathVec;}
global proc BE_CrossProduct()          {createNode  BE_CrossProduct;}
global proc BE_DotProduct()            {createNode  BE_DotProduct;}
global proc BE_Distance()              {createNode  BE_Distance;}
global proc BE_Length()                {createNode  BE_Length;}
global proc BE_Normalize()             {createNode  BE_Normalize;}
global proc BE_DeletePointByValue(){createNode  BE_DeletePointByValue;}
global proc BE_Fit(){createNode  BE_Fit;}
global proc BE_Ramp(){createNode  BE_Ramp;}
global proc BE_Switch(){createNode  BE_Switch;}
global proc BE_FloatToVector(){createNode  BE_FloatToVector;}
global proc BE_VectorToFloat(){createNode  BE_VectorToFloat;}
global proc BE_ConvertScalarArray(){createNode  BE_ConvertScalarArray;}
global proc BE_GetPoints(){createNode  BE_GetPoints;}
global proc BE_SetPoints(){createNode  BE_SetPoints;}
global proc BE_SeExprPoint(){createNode  BE_SeExprPoint;}
global proc BE_TurbulenceNoise(){createNode  BE_TurbulenceNoise;}
global proc BE_AntiAliasedFlowNoise(){createNode  BE_AntiAliasedFlowNoise;}
global proc BE_fbmNoise(){createNode  BE_fbmNoise;}
global proc BE_WorleyNoise(){createNode  BE_WorleyNoise;}
global proc BE_Velocity(){createNode  BE_Velocity;}

global proc BE_BifrostFileToParticles()
{
	string $shapeToConnect;
	string $cmd = "nParticle";
	string $particleNames[] = evalEcho($cmd);
	$shapeToConnect = $particleNames[0];

	if (`optionVar -query emitterDieOnExit`)
	{
		setAttr ($particleNames[0]+".dieOnEmissionVolumeExit") 1;
	}

    string $connectCmd = "connectDynamic ";
    $connectCmd = $connectCmd + $shapeToConnect + "; ";

    string $BifrostFileToArray = `createNode  BE_BifrostFileToArray`;

    connectAttr ($BifrostFileToArray+".outPosition") ($shapeToConnect+".positions");
    connectAttr ($BifrostFileToArray+".outDynamicArray") ($shapeToConnect+".cacheArrayData");
    setAttr  ($shapeToConnect+".playFromCache") 1;
    setAttr  ($shapeToConnect+".colorInput") 3;
    setAttr  ($shapeToConnect+".colorInputMax") 20;

    setAttr  ($shapeToConnect+".color[1].color_Color") -type double3 1 1 1;
    setAttr  ($shapeToConnect+".color[1].color_Position") 1.0;
    setAttr  ($shapeToConnect+".color[1].color_Interp ") 1;

    setAttr  ($shapeToConnect+".color[0].color_Position") 0;
    setAttr  ($shapeToConnect+".color[0].color_Color") -type double3 0 0 1;

    select $BifrostFileToArray;
}


global proc InsertFracture()
{
	string $selFracture[]=`ls -sl`;
	int $lastSelect = size($selFracture[0]);
	string $tipe;
	int $frgCount;
	if($lastSelect > 0)
	$tipe = `nodeType $selFracture[0]`;
	$frgCount = getAttr($selFracture[0]+".FragmentCount");

	if($tipe == "BE_VDBFracture")
	{
		string $Convert = `createNode  BE_VDBConvertVDB`;
		connectAttr -force ($selFracture[0]+".VdbOutput") ($Convert+".vdbInput");
		setAttr ($Convert+".ConvertTo") 1;
		setAttr ($Convert+".InvertNormal") 1;
		print( $selFracture[0]);
		for($i = 0;$i < $frgCount;++$i)
		{
			string $Mesh = `createNode  mesh`;
			sets -e -forceElement initialShadingGroup;
			connectAttr -force BE_VDBConvertVDB1.meshOutput[$i] ($Mesh+".inMesh");
		}
	}
	else
	{
		warning "No BE_VDBFracture selected";
	}
}"""
		mm.eval(s)

	def printHostId(self, *arg): import uuid; print('SOuP: Host ID (mac address): %012x' % uuid.getnode())