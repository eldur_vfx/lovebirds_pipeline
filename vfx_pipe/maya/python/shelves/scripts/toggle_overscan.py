import maya.cmds as cmds

def main():
    if(cmds.getAttr('defaultResolution.width') == 1998):
        wneu = cmds.getAttr('defaultResolution.width')*1.1
        hneu = cmds.getAttr('defaultResolution.height')*1.1

        cmds.setAttr('defaultResolution.width',wneu)
        cmds.setAttr('defaultResolution.height',hneu)
        print "Render Resolutiuon set to 2k flat with 10% overscan: 2198 x 1188"
    else:
        cmds.setAttr('defaultResolution.width',1998)
        cmds.setAttr('defaultResolution.height',1080)
        print "Render Resolution set to 2k flat: 1998 x 1080"