#coding: utf8

# Copyright (C) by Adrian Sochacki, since 2019. All rights reserved.
#
# Description: Easily create turntables with only a few clicks!
#
# How to use: Run the script, change some settings, select an object and hit 'Create'
# 'Directory to background image', change this to the path of your background image
# 'DOF', check this to enable depth of field
# 'F-stop', changes the f-stop value
# 'Turn camera', enable this to create an animation for the camera where it turns around your selected object
# 'Turn background', enable this to create an animation for the background where it turns around your selected object
# 'Direction', change the direction of the animation
# 'Animation length', change the length of the rotation animation
#
# Version: 1.1.4
#

import maya.cmds as cmds
from functools import partial
import sys
import math

def onMayaDroppedPythonFile(*args):
    TurntableWindow()


def TurnClockwiseOn(*args):
    cmds.checkBox('clockwise_check', edit = True, value = True)


def TurnClockwiseOff(*args):
    cmds.checkBox('clockwise_check', edit = True, value = False)


def TurnCounterClockwiseOn(*args):
    cmds.checkBox('counterClockwise_check', edit = True, value = True)


def TurnCounterClockwiseOff(*args):
    cmds.checkBox('counterClockwise_check', edit = True, value = False)


def SwitchDirectionCheckbox(*args):
    if cmds.checkBox('turnCamera_check', query = True, value = True) or cmds.checkBox('turnBG_check', query = True, value = True):
        cmds.checkBox('clockwise_check', edit = True, enable = True)
        cmds.checkBox('counterClockwise_check', edit = True, enable = True)

    if not cmds.checkBox('turnCamera_check', query = True, value = True) and not cmds.checkBox('turnBG_check', query = True, value = True):
        cmds.checkBox('clockwise_check', edit = True, enable = False)
        cmds.checkBox('counterClockwise_check', edit = True, enable = False)


def SwitchBackgroundTurn(*args):
    if cmds.checkBox('createBG_check', query = True, value = True):
        cmds.checkBox('turnBG_check', edit = True, enable = True)
        cmds.text('turnBG_text', edit = True, enable = True)
        cmds.textField('hdriDirectory_textfield', edit = True, enable = True)
        cmds.button('browse_button', edit = True, enable = True)

    if not cmds.checkBox('createBG_check', query = True, value = True):
        cmds.checkBox('turnBG_check', edit = True, enable = False)
        cmds.text('turnBG_text', edit = True, enable = False)
        cmds.textField('hdriDirectory_textfield', edit = True, enable = False)
        cmds.button('browse_button', edit = True, enable = False)


def TurntableWindow(*args):
    winID = 'schocki_turntableUI'
    if cmds.window(winID, exists = True):
        cmds.deleteUI(winID)

    if cmds.windowPref(winID, exists = True):
        topLeftCorner = cmds.windowPref(winID, query = True, topLeftCorner = True)
        cmds.windowPref(winID, remove = True)
    else:
        topLeftCorner = (197, 442)

    window = cmds.window(winID, title = 'Turntable Creator', width = 300, height = 200, topLeftCorner = topLeftCorner)
    cmds.columnLayout()

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout(numberOfColumns = 5)
    cmds.separator(width = 10, style = 'none')
    cmds.textField('hdriDirectory_textfield', text = 'Directory to background image', width = 191, annotation = 'If left empty no background image will be used')
    cmds.separator(width = 5, style = 'none')
    cmds.button('browse_button', label = 'Browse', width = 80, height = 23, annotation = 'Browse for an image file of one of the following formats: \n *.jpg *.jpeg *.png *.hdr *.psd *.tiff *.tif *.exr *.bmp *.gif')
    cmds.button('browse_button', edit = True, command = SearchForHDRFile)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout(numberOfColumns = 7, annotation = 'Focal length, if left empty 50mm will be used.')
    cmds.separator(width = 12, style = 'none')
    cmds.text('focalLengthText', label = 'Focal length :')
    cmds.separator(width = 1, style = 'none')
    cmds.textField('focalLength_textfield', text = 'Focal Length', width = 110)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout('dofRowLayout', numberOfColumns = 4, annotation = 'Check to use depth of field.')
    cmds.separator(width = 12, style = 'none')
    cmds.text(label = 'DOF : ')
    cmds.checkBox('DOF_check', label = '')
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout('fstopRowLayout', numberOfColumns = 5, annotation = 'F stop, if left empty 5.6 will be used')
    cmds.separator(width = 12, style = 'none')
    cmds.text('fstopText', label = 'F-Stop :')
    cmds.separator(width = 1, style = 'none')
    cmds.textField('fstop_textfield', text = 'F Stop')
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout(numberOfColumns = 4, annotation = 'Check to create a rotation animation of the camera that turns around your selected object')
    cmds.separator(width = 12, style = 'none')
    cmds.text(label = 'Turn camera: ')
    cmds.checkBox('turnCamera_check', label = '', changeCommand = SwitchDirectionCheckbox)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout(numberOfColumns = 4, annotation = 'Check to create a background around your selected object')
    cmds.separator(width = 12, style = 'none')
    cmds.text('createBG_text', label = 'Create background: ')
    cmds.checkBox('createBG_check', label = '', changeCommand = SwitchBackgroundTurn)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout(numberOfColumns = 4, annotation = 'Check to create a rotation animation of the background around your selected object')
    cmds.separator(width = 12, style = 'none')
    cmds.text('turnBG_text',label = 'Turn background: ')
    cmds.checkBox('turnBG_check', label = '', changeCommand = SwitchDirectionCheckbox)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout(numberOfColumns = 5, annotation = 'Animation direction: clockwise or counter-clockwise')
    cmds.separator(width = 12, style = 'none')
    cmds.text(label = 'Direction: ')
    cmds.checkBox('clockwise_check', label = 'clockwise', onCommand = TurnCounterClockwiseOff, offCommand = TurnCounterClockwiseOn)
    cmds.checkBox('counterClockwise_check', label = 'counter-clockwise', onCommand = TurnClockwiseOff, offCommand = TurnClockwiseOn)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout('animationLengthRowLayout', numberOfColumns = 6, annotation = 'If left empty animation will be 120 frames long')
    cmds.separator(width = 12, style = 'none')
    cmds.text('animationLengthText', label = 'Animationlength :')
    cmds.separator(width = 1, style = 'none')
    cmds.textField('animationLength_textfield', text = 'Animation Length', width = 110)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.rowLayout(numberOfColumns = 3, annotation = 'Create a turntable')
    cmds.separator(width = 10, style = 'none')
    cmds.button(label = 'Create', width = 280, command = CreateTurntable)
    cmds.separator(width = 10, style = 'none')
    cmds.setParent('..')

    #Space
    cmds.separator(height = 10, style = 'none')

    cmds.setParent('..')

    #Query all optionvariable values:
    imageDirectory = cmds.optionVar(query = 'turntableCreator_directory')
    focalLengthText = cmds.optionVar(query = 'turntableCreator_focalLength')
    DOF_value = cmds.optionVar(query = 'turntableCreator_DOF')
    fstop_value = cmds.optionVar(query = 'turntableCreator_fStop')
    turnCamera_value = cmds.optionVar(query = 'turntableCreator_turnCamera')
    createBG_value = cmds.optionVar(query = 'turntableCreator_createBackground')
    turnBG_value = cmds.optionVar(query = 'turntableCreator_turnBackground')
    direction_value = cmds.optionVar(query = 'turntableCreator_direction')
    animLength_value = cmds.optionVar(query = 'turntableCreator_animationLength')

    #Set all optionvariables values:
    if imageDirectory == 0 or imageDirectory == '':
        cmds.textField('hdriDirectory_textfield', edit = True, text = 'Directory to background image')
    else:
        try:
            cmds.textField('hdriDirectory_textfield', edit = True, text = imageDirectory)
        except (RuntimeError, TypeError, NameError, ValueError):
            pass

    if focalLengthText == 0 or focalLengthText == '':
        cmds.textField('focalLength_textfield', edit = True, text = 'Focal Length')
    else:
        try:
            cmds.textField('focalLength_textfield', edit = True, text = focalLengthText)
        except (RuntimeError, TypeError, NameError, ValueError):
            pass

    if DOF_value == 1:
        cmds.checkBox('DOF_check', edit = True, value = True)

    if fstop_value == 0 or fstop_value == '':
        cmds.textField('fstop_textfield', edit = True, text = 'F Stop')
    else:
        try:
            cmds.textField('fstop_textfield', edit = True, text = cmds.optionVar(query = 'turntableCreator_fStop'))
        except (RuntimeError, TypeError, NameError, ValueError):
            pass

    if turnCamera_value == 1:
        cmds.checkBox('turnCamera_check', edit = True, value = turnCamera_value)

    if createBG_value == 1:
        cmds.checkBox('createBG_check', edit = True, value = createBG_value)

    if turnBG_value == 1:
        cmds.checkBox('turnBG_check', edit = True, value = turnBG_value)

    if direction_value == 0:
        cmds.checkBox('clockwise_check', edit = True, value = False)
        cmds.checkBox('counterClockwise_check', edit = True, value = False)

    if direction_value == 1:
        cmds.checkBox('clockwise_check', edit = True, value = True)
        cmds.checkBox('counterClockwise_check', edit = True, value = False)

    if direction_value == 2:
        cmds.checkBox('clockwise_check', edit = True, value = False)
        cmds.checkBox('counterClockwise_check', edit = True, value = True)

    if animLength_value == 0 or animLength_value == '':
        cmds.textField('animationLength_textfield', edit = True, text = 'Animation Length')
    else:
        try:
            cmds.textField('animationLength_textfield', edit = True, text = animLength_value)
        except (RuntimeError, TypeError, NameError, ValueError):
            pass

    SwitchBackgroundTurn()
    SwitchDirectionCheckbox()
    cmds.showWindow()


def CreateTurntable(*args):
    sel = cmds.ls(sl=True)

    if len(sel) == 0:
        sys.exit('You have nothing selected. Please select at least one object.\n')

    if cmds.checkBox('turnCamera_check', query = True, value = True) and not cmds.checkBox('clockwise_check', query = True, value = True) and not cmds.checkBox('counterClockwise_check', query = True, value = True):
        sys.exit('You have selected that you want to turn the camera but not in which direction. Please select first a animation type and a direction.\n')

    if cmds.checkBox('turnBG_check', query = True, value = True) and not cmds.checkBox('clockwise_check', query = True, value = True) and not cmds.checkBox('counterClockwise_check', query = True, value = True):
        sys.exit('You have selected that you want to turn the background but not in which direction. Please select first a animation type and a direction.\n')

    if cmds.checkBox('clockwise_check', query = True, value = True) and cmds.checkBox('counterClockwise_check', query = True, value = True):
        sys.exit('Unexpected Error: Somehow both directions are enabled, that should not have been happened, please contact me. - #' + str(cf.f_lineno) + '\n')

    if not cmds.checkBox('clockwise_check', query = True, value = True) and not cmds.checkBox('counterClockwise_check', query = True, value = True):
        sys.exit('You have not selected in which direction the turntable should be turning. Please select a direction.\n')

    bBox = cmds.exactWorldBoundingBox(sel)
    v1 = [bBox[0], bBox[1], bBox[2]]
    v2 = [bBox[3], bBox[4], bBox[5]]
    v3 = [v2[0]- v1[0], v2[1]-v1[1], v2[2]-v1[2]]
    vLength = math.sqrt(v3[0]**2 + v3[1]**2 + v3[2]**2)

    if cmds.pluginInfo('mtoa.mll', query = True, loaded = True):
        arnoldPlugin = True
    else:
        arnoldPlugin = False
        sys.stdout.write('The plugin Arnold is not loaded, if you want to use Arnold Lights, open the Plugin Manager, make sure Maya recognized and loaded the plugin and try again.\n')

    cmds.undoInfo(chunkName = 'turntableCreator', openChunk = True)

    if cmds.checkBox('turnCamera_check', query = True, value = True):
        circle = cmds.circle(radius = bBox[3] * 9, sections = 50)
        cmds.setAttr(circle[0] + '.rotateX', 90)
        cmds.reverseCurve(circle[0])

    turnCamera = cmds.camera()
    cameraShape = turnCamera[1]

    textFieldInput = cmds.textField('focalLength_textfield', query = True, text = True)
    textFieldInputStrip = ''.join(textFieldInput.split())
    focalLength = cmds.textField('focalLength_textfield', query = True, text = True)
    if focalLength == 'Focal Length' or focalLength == '' or focalLength == ' ' or focalLength == None:
        cmds.camera(cameraShape, edit = True, focalLength = 50)
    else:
        if int(textFieldInputStrip) <= 0:
            cmds.camera(cameraShape, edit = True, focalLength = 2.5)
        else:
            try:
                cmds.camera(cameraShape, edit = True, focalLength = int(textFieldInputStrip))
            except (RuntimeError, TypeError, NameError, ValueError):
                cmds.camera(cameraShape, edit = True, focalLength = 50)

    #DOF:
    if cmds.checkBox('DOF_check', query = True, value = True):
        DOF_value = 1
        if arnoldPlugin:
            cmds.setAttr(cameraShape + '.aiEnableDOF', 1)

        cmds.camera(cameraShape, edit = True, depthOfField = True)
    else:
        DOF_value = 0

    #F Stop:
    fstopInput = cmds.textField('fstop_textfield', query = True, text = True)
    fstopInputStrip = ''.join(fstopInput.split())
    fStop = cmds.textField('fstop_textfield', query = True, text = True)
    if fStop == 'F Stop' or fStop == '' or fStop == ' ' or fStop == None:
        cmds.camera(cameraShape, edit = True, fStop = 5.6)
        if arnoldPlugin:
            cmds.setAttr(cameraShape + '.aiApertureSize', ((50.0/5.6)/2)/10)
    else:
        if float(fstopInputStrip) <= 0:
            cmds.camera(cameraShape, edit = True, fStop = 1)
            if arnoldPlugin:
                cmds.setAttr(cameraShape + '.aiApertureSize', ((50.0/1)/2)/10)
        elif float(fstopInputStrip) >= 64:
            cmds.camera(cameraShape, edit = True, fStop = 64)
            if arnoldPlugin:
                cmds.setAttr(cameraShape + '.aiApertureSize', 20)
        else:
            if arnoldPlugin:
                try:
                    cmds.setAttr(cameraShape + '.aiApertureSize', (((50.0/float(fstopInputStrip))/2)/10))
                except (RuntimeError, TypeError, NameError, ValueError):
                    cmds.setAttr(cameraShape + '.aiApertureSize', ((50.0/5.6)/2)/10)

            try:
                cmds.camera(cameraShape, edit = True, fStop = float(fstopInputStrip))
            except (RuntimeError, TypeError, NameError, ValueError):
                cmds.camera(cameraShape, edit = True, fStop = 5.6)

    #Direction:
    direction = 'None'
    if 	cmds.checkBox('clockwise_check', query = True, value = True) and not cmds.checkBox('counterClockwise_check', query = True, value = True):
        direction = 'Clockwise'
        direction_value = 1

    if 	not cmds.checkBox('clockwise_check', query = True, value = True) and cmds.checkBox('counterClockwise_check', query = True, value = True):
        direction = 'Counter clockwise'
        direction_value = 2

    #Turn camera:
    animationLengthInput = cmds.textField('animationLength_textfield', query = True, text = True)
    animationLengthInputStrip = ''.join(animationLengthInput.split())
    try:
        animationLengthInputStrip = int(animationLengthInputStrip)
    except (RuntimeError, TypeError, NameError, ValueError):
        pass

    if type(animationLengthInputStrip) == int:
        animLengthInt = True
    else:
        animLengthInt = False
        animationLengthInputStrip = 120

    if animationLengthInputStrip <= 0 or animationLengthInputStrip == '' or animationLengthInputStrip == ' ' or animationLengthInputStrip == None:
        animationLengthInputStrip = 120

    animLength_value = str(animationLengthInputStrip)

    if cmds.checkBox('turnCamera_check', query = True, value = True):
        turnCamera_value = 1
        cmds.undoInfo(stateWithoutFlush = False)
        cmds.pathAnimation(turnCamera[0], circle[0], fractionMode = True, follow = True, followAxis = 'x', upAxis = 'y',
        worldUpType = 'vector', worldUpVector = [0, 1, 0], inverseUp = False, inverseFront = False, bank = False,
        startTimeU = 1, endTimeU = animationLengthInputStrip)
        cmds.undoInfo(stateWithoutFlush = True)

        con = cmds.listConnections(turnCamera)
        motionpath = con[6]
        if direction == 'Counter clockwise':
            pass
        if direction == 'Clockwise':
            cmds.keyframe(motionpath, edit = True, time=(0, 1), valueChange = 1)
            cmds.keyframe(motionpath, edit = True, time=(animationLengthInputStrip - 1, animationLengthInputStrip), valueChange = 0)

        cmds.keyTangent(motionpath, inTangentType = 'linear', outTangentType = 'linear', time = (0, animationLengthInputStrip + 1))
        cmds.playbackOptions(minTime = 0, animationStartTime = 0)
        cmds.playbackOptions(maxTime = animationLengthInputStrip - 1, animationEndTime = animationLengthInputStrip - 1)

    else:
        cmds.setAttr(turnCamera[0] + '.translateZ', bBox[3] * 9)
        turnCamera_value = 0

    if cmds.checkBox('createBG_check', query = True, value = True):
        if arnoldPlugin:
            createBG_value = 1
            #Turn Background:
            import mtoa.utils as mutils
            BG = mutils.createLocator('aiSkyDomeLight', asLight=True)
            if cmds.checkBox('turnBG_check', query = True, value = True):
                turnBG_value = 1
                if cmds.checkBox('turnCamera_check', query = True, value = True):
                    if direction == 'Clockwise':
                        cmds.setKeyframe(BG, time = animationLengthInputStrip, attribute = 'rotateY', value = 0)
                        cmds.setKeyframe(BG, time = animationLengthInputStrip * 2, attribute = 'rotateY', value = -360)
                        cmds.keyTangent(BG, inTangentType = 'linear', outTangentType = 'linear', time = (animationLengthInputStrip, animationLengthInputStrip * 2))

                    if direction == 'Counter clockwise':
                        cmds.setKeyframe(BG, time = animationLengthInputStrip, attribute = 'rotateY', value = 0)
                        cmds.setKeyframe(BG, time = animationLengthInputStrip * 2, attribute = 'rotateY', value = 360)
                        cmds.keyTangent(BG, inTangentType = 'linear', outTangentType = 'linear', time = (animationLengthInputStrip, animationLengthInputStrip * 2))
                    cmds.playbackOptions(maxTime = animationLengthInputStrip * 2, animationEndTime = animationLengthInputStrip * 2)

                else:
                    if direction == 'Clockwise':
                        cmds.setKeyframe(BG, time = 1, attribute = 'rotateY', value = 0)
                        cmds.setKeyframe(BG, time = animationLengthInputStrip, attribute = 'rotateY', value = -360)
                        cmds.keyTangent(BG, inTangentType = 'linear', outTangentType = 'linear', time = (1, animationLengthInputStrip))

                    if direction == 'Counter clockwise':
                        cmds.setKeyframe(BG, time = 1, attribute = 'rotateY', value = 0)
                        cmds.setKeyframe(BG, time = animationLengthInputStrip, attribute = 'rotateY', value = 360)
                        cmds.keyTangent(BG, inTangentType = 'linear', outTangentType = 'linear', time = (1, animationLengthInputStrip))
                    cmds.playbackOptions(maxTime = animationLengthInputStrip, animationEndTime = animationLengthInputStrip)
            else:
                turnBG_value = 0

            #Set image file as BG:
            imageDirectoryInput = cmds.textField('hdriDirectory_textfield', query = True, text = True)
            imageDirectoryInputStrip = imageDirectoryInput.strip()
            if imageDirectoryInputStrip == 'Directory to background image' or imageDirectoryInputStrip == '' or imageDirectoryInputStrip == ' ' or imageDirectoryInputStrip == None:
                pass
            else:
                try:
                    file = cmds.shadingNode('file', asTexture = True)
                    place2D = cmds.shadingNode('place2dTexture', asUtility = True)
                    cmds.connectAttr(place2D + '.coverage', file + '.coverage', force = True)
                    cmds.connectAttr(place2D + '.mirrorU', file + '.mirrorU', force = True)
                    cmds.connectAttr(place2D + '.mirrorV', file + '.mirrorV', force = True)
                    cmds.connectAttr(place2D + '.noiseUV', file + '.noiseUV', force = True)
                    cmds.connectAttr(place2D + '.offset', file + '.offset', force = True)
                    cmds.connectAttr(place2D + '.outUV', file + '.uvCoord', force = True)
                    cmds.connectAttr(place2D + '.outUvFilterSize', file + '.uvFilterSize', force = True)
                    cmds.connectAttr(place2D + '.repeatUV', file + '.repeatUV', force = True)
                    cmds.connectAttr(place2D + '.rotateFrame', file + '.rotateFrame', force = True)
                    cmds.connectAttr(place2D + '.rotateUV', file + '.rotateUV', force = True)
                    cmds.connectAttr(place2D + '.stagger', file + '.stagger', force = True)
                    cmds.connectAttr(place2D + '.translateFrame', file + '.translateFrame', force = True)
                    cmds.connectAttr(place2D + '.vertexCameraOne', file + '.vertexCameraOne', force = True)
                    cmds.connectAttr(place2D + '.vertexUvOne', file + '.vertexUvOne', force = True)
                    cmds.connectAttr(place2D + '.vertexUvTwo', file + '.vertexUvTwo', force = True)
                    cmds.connectAttr(place2D + '.vertexUvThree', file + '.vertexUvThree', force = True)
                    cmds.connectAttr(place2D + '.wrapU', file + '.wrapU', force = True)
                    cmds.connectAttr(place2D + '.wrapV', file + '.wrapV', force = True)
                    cmds.connectAttr(file + '.outColor', BG[1] + '.color', force = True)
                    cmds.setAttr(file + '.fileTextureName', cmds.textField('hdriDirectory_textfield', query = True, text = True), type = 'string')

                except (RuntimeError, TypeError, NameError, ValueError):
                    sys.exit('Something went wrong while setting your image file as the background image. Try again.\n')
        else:
            createBG_value = 0
            if cmds.checkBox('turnBG_check', query = True, value = True):
                turnBG_value = 1
            else:
                turnBG_value = 0
            cmds.warning('Arnold Plugin not loaded, Native Maya does not have a sky dome to create and rotate around the background.')
    else:
        createBG_value = 0
        if cmds.checkBox('turnBG_check', query = True, value = True):
            turnBG_value = 1
        else:
            turnBG_value = 0

    #Distance between Camera:
    distanceNode = cmds.shadingNode('distanceBetween', asUtility = True)
    loc1 = cmds.spaceLocator()
    locShape1 = cmds.listRelatives(loc1[0], shapes = True)
    loc2 = cmds.spaceLocator()
    locShape2 = cmds.listRelatives(loc2[0], shapes = True)

    #Podest:
    podest = cmds.polyCylinder(axis = [0, 1, 0], height = bBox[4] / 10.0, radius = bBox[3] * 2.5, subdivisionsX = 60, subdivisionsZ = 1)
    cmds.setAttr(podest[0] + '.translateY', bBox[1] - (bBox[4] / 10.0) / 2)

    cmds.parent(loc1[0], turnCamera[0])
    cmds.parent(loc2[0], sel[0])
    cmds.setAttr(loc1[0] + '.translateX', 0)
    cmds.setAttr(loc1[0] + '.translateY', 0)
    cmds.setAttr(loc1[0] + '.translateZ', 0)
    cmds.setAttr(loc2[0] + '.translateX', 0)
    cmds.setAttr(loc2[0] + '.translateY', 0)
    cmds.setAttr(loc2[0] + '.translateZ', 0)
    cmds.connectAttr(locShape1[0] + '.worldPosition', distanceNode + '.point1')
    cmds.connectAttr(locShape2[0] + '.worldPosition', distanceNode + '.point2')

    distance = cmds.getAttr(distanceNode + '.distance')
    if distance == 0.0:
        distance = 0.01
    cmds.camera(cameraShape, edit = True, focusDistance = distance)

    if arnoldPlugin:
        cmds.setAttr(cameraShape + '.aiFocusDistance', distance)

    cmds.delete(distanceNode)
    cmds.delete(loc1, shape = True)
    cmds.delete(loc2, shape = True)

    if cmds.checkBox('turnCamera_check', query = True, value = True):
        cmds.select(circle)

    if cmds.checkBox('createBG_check', query = True, value = True):
        if arnoldPlugin:
            if cmds.checkBox('turnCamera_check', query = True, value = True):
                cmds.select(BG, add = True)
            else:
                cmds.select(BG)

    cmds.select(turnCamera, add = True)
    cmds.select(podest, add = True)

    turntableContainer = cmds.group()
    if arnoldPlugin:
        sys.stdout.write('New turntable created with following attributes:\n' +
            'Focal length: ' + str(cmds.camera(cameraShape, query = True, focalLength = True))
            + '\n'
            + 'Depth of field: ' + str(cmds.checkBox('DOF_check', query = True, value = True))
            + '\n'
            + 'F Stop: ' + str(cmds.camera(cameraShape, query = True, fStop = True))
            + '\n'
            + 'Turn the camera: ' + str(cmds.checkBox('turnCamera_check', query = True, value = True))
            + '\n'
            + 'Create background: ' + str(cmds.checkBox('createBG_check', query = True, value = True))
            + '\n'
            + 'Turn the background: ' + str(cmds.checkBox('turnBG_check', query = True, value = True))
            + '\n'
            + 'Direction: ' + direction
            + '\n'
            + 'Animation length: ' + str(animationLengthInputStrip)
            + '\n')
    else:
        sys.stdout.write('New turntable created with following attributes:\n' +
        'Focal length: ' + str(cmds.camera(cameraShape, query = True, focalLength = True))
        + '\n'
        + 'Depth of field: ' + str(cmds.checkBox('DOF_check', query = True, value = True))
        + '\n'
        + 'F Stop: ' + str(cmds.camera(cameraShape, query = True, fStop = True))
        + '\n'
        + 'Turn the camera: ' + str(cmds.checkBox('turnCamera_check', query = True, value = True))
        + '\n'
        + 'Create background: ' + str(cmds.checkBox('createBG_check', query = True, value = True)) + ', but the Arnold plugin is not loaded.'
        + '\n'
        + 'Turn the background: ' + str(cmds.checkBox('turnBG_check', query = True, value = True)) + ', but the Arnold plugin is not loaded.'
        + '\n'
        + 'Direction: ' + direction
        + '\n'
        + 'Animation length: ' + str(animationLengthInputStrip)
        + '\n')

    if cmds.checkBox('turnCamera_check', query = True, value = True):
        cmds.rename(circle[0], 'turntablePath')

    if cmds.checkBox('createBG_check', query = True, value = True):
        if arnoldPlugin:
            cmds.rename(BG[1], 'turntableBackground')

    cmds.rename(podest[0], 'turntablePedestal')
    cmds.rename(turnCamera[0], 'turntableCamera')
    cmds.rename(turntableContainer, 'turntable_GRP')
    cmds.undoInfo(chunkName = 'turntableCreator', closeChunk = True)

    cmds.optionVar(stringValue = ['turntableCreator_directory', cmds.textField('hdriDirectory_textfield', query = True, text = True)])
    cmds.optionVar(stringValue = ['turntableCreator_focalLength', cmds.textField('focalLength_textfield', query = True, text = True)])
    cmds.optionVar(intValue = ['turntableCreator_DOF', DOF_value])
    cmds.optionVar(stringValue = ['turntableCreator_fStop', cmds.textField('fstop_textfield', query = True, text = True)])
    cmds.optionVar(intValue = ['turntableCreator_turnCamera', turnCamera_value])
    cmds.optionVar(intValue = ['turntableCreator_createBackground', createBG_value])
    cmds.optionVar(intValue = ['turntableCreator_turnBackground', turnBG_value])
    cmds.optionVar(intValue = ['turntableCreator_direction', direction_value])
    cmds.optionVar(stringValue = ['turntableCreator_animationLength', animLength_value])


def SearchForHDRFile(*args):
    pictureFilter = 'Common picture file formats (*.jpg *.jpeg *.png *.hdr *.psd *.tiff *.tif *.exr *.bmp *.gif);; JPEG(*.jpg *.jpeg);; PNG(*.png);; HDR(*.hdr);; PSD(*.psd);; TIFF(*.tiff *.tif);; EXR(*.exr);; BMP(*.bmp);; GIF(*.gif);;'
    HDRFile = cmds.fileDialog2(dialogStyle = 2, fileFilter = pictureFilter, caption = 'Choose a background image', fileMode = 1, okCaption = 'Choose')
    if HDRFile == None or HDRFile[0] == None:
        sys.exit('You or something else canceled the selection of a directory. Did you only see empty folders? Make sure you have the requiered permissions for the folder.\n')
    imageDirectory = HDRFile[0]
    cmds.textField('hdriDirectory_textfield', edit = True, text = imageDirectory)


if __name__ == '__main__':
    if not cmds.about(batch = True):
        TurntableWindow()