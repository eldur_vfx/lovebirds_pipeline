#Arnold Subdivison version 1.0.0
#For Maya. Tested in Maya 2014 &amp; 2015 with Arnold 4.2.9.0
#Bryanna London www.bryannalondon.com
#!/usr/bin/env python
 
import maya.cmds as cmds

class SubdivGui():
    def createGUI(self):
        #window layout
        arnoldSubdWindow= cmds.window('arnoldSubdWindow',title="Add SubDivisions", rtf=True)
        cmds.columnLayout(adjustableColumn= True, rowSpacing= 3)
        cmds.optionMenu('subdType', label='Type')
        cmds.menuItem( label='Catclark')
        cmds.menuItem( label='None')
        cmds.menuItem( label='Linear')
        cmds.setParent('..')
        cmds.intFieldGrp('iterations', numberOfFields=1, label="Iterations", value1= 2)
        cmds.rowColumnLayout(numberOfRows = 2)
        cmds.button( label='Run', width= 250, command=('setSubdivision()'))
        cmds.columnLayout()
        cmds.button( label='Close', width= 250, command=('cmds.deleteUI("' + arnoldSubdWindow + '", window=True)'))
        cmds.setParent('..')
        cmds.showWindow('arnoldSubdWindow')
    
    #Query Values
    global queryValues
    def queryValues():
    
        x= cmds.intFieldGrp('iterations',query=True, value=True)
        
        #query option menu
        selectedMenuItem = cmds.optionMenu('subdType', query = True, value = True)
        #print selectedMenuItem
        
        if selectedMenuItem == 'Catclark':
            menuValue = 1
        
        if selectedMenuItem == 'None':
            menuValue = 0
        
        if selectedMenuItem == 'Linear':
            menuValue = 2
        
        #query intFieldGrp
        queriedValuesX = x
        
        #Return Queried Values
        return queriedValuesX[0], menuValue
    
    #Set Subdivision Values
    global setSubdivision
    def setSubdivision():
    
        #Get GUI values
        queriedValuesX, menuValue = queryValues()
    
        selectedObjs = cmds.ls(selection = True, dag = True, lf = True, visible=True, type = 'mesh')
    
        for sel in selectedObjs:
            cmds.setAttr('%s.aiSubdivType' %(sel), menuValue)
            cmds.setAttr('%s.aiSubdivIterations' %(sel), queriedValuesX)
 
#Run Script
def main():
    #Create GUI
    if cmds.window('arnoldSubdWindow', exists=True):
        cmds.deleteUI('arnoldSubdWindow')
    SubdivGui().createGUI()