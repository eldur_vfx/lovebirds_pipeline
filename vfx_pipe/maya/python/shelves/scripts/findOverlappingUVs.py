import pymel.core as pm

def select_overlapping_faces(sel=None):
    face_list = []
    if sel is None:
        for shape in pm.ls(type="mesh"):
            for face in shape.f:
                face_list.append(face)
    else:
        for shape in pm.listRelatives(sel, shapes=1):
            for face in shape.f:
                face_list.append(face)
            
    overlapping_faces = pm.polyUVOverlap(face_list, oc=True)

    print overlapping_faces
    
    overlapping_objects = []
    if overlapping_faces is not None:
        for p in overlapping_faces:
            node = pm.listRelatives(p, parent=True)[0].name()
            overlapping_objects.append(p)
            
        pm.select(overlapping_objects)
    else:
        pm.select(clear=True)
        
def main():	
    select_overlapping_faces(pm.ls(sl=1))
