import maya.cmds as cmds

def main():
    sel=cmds.ls(sl=1)
    cmds.select(sel,hi=True)
    currShape = cmds.ls( sl=True, shapes=True)
    oldName = sel[0]
    #check if selected shape is of type "camera"
    if(cmds.objectType(currShape, isType="camera")):
        #check if selected cam doesnt contain "_overscan" already
        if(oldName.find('_overscan') == -1):
            dup = cmds.duplicate(sel, ic=True )
            cmds.select( dup, r=True )
            #rename new cam
            newName = oldName + "_overscan"
            cmds.rename(newName)
            cmds.setAttr( newName + '.cameraScale', 1.1)
            #set res to overscan, if its not already set
            if(cmds.getAttr('defaultResolution.width') == 1998):
            #if(cmds.getAttr('defaultResolution.width') == 1998 or cmds.getAttr('defaultResolution.width') == 2880):

                wneu = cmds.getAttr('defaultResolution.width')*1.1
                hneu = cmds.getAttr('defaultResolution.height')*1.1

                cmds.setAttr('defaultResolution.width',wneu)
                cmds.setAttr('defaultResolution.height',hneu)
        else:
            print "Selected Camera is already an overscan Camera"
            cmds.confirmDialog(title='set overscan tool', message='The selected camera is already an overscan Camera!')
    else:
        print "Please select a valid Camera"
        cmds.confirmDialog(title='set overscan tool', message='Please select a valid Camera!')