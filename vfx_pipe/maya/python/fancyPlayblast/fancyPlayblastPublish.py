def fancyPlayblastPublish():
    # need to have an engine running in a context where the publisher has been
    # configured.
    engine = sgtk.platform.current_engine()

    # get the publish app instance from the engine's list of configured apps
    publish_app = engine.apps.get("tk-multi-publish2")

    # ensure we have the publisher instance.
    if not publish_app:
        raise Exception("The publisher is not configured for this context.")

    # create a new publish manager instance
    manager = publish_app.create_publish_manager()

    # now we can run the collector that is configured for this context
    manager.collect_session()

    # collect some external files to publish
    manager.collect_files([path1, path2, path3])

    # validate the items to publish
    tasks_failed_validation = manager.validate()

    # oops, some tasks are invalid. see if they can be fixed
    if tasks_failed_validation:
        fix_invalid_tasks(tasks_failed_validation)
        # try again here or bail

    # all good. let's publish and finalize
    try:
        manager.publish()
        # If a plugin needed to version up a file name after publish
        # it would be done in the finalize.
        manager.finalize()
    except Exception as error:
        logger.error("There was trouble trying to publish!")
        logger.error("Error: %s", error)