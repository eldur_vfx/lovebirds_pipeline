# custom menu

import pymel.core as pm
import os
import sys
import json

#################################################################################################
PIPE_PIPELINEPATH = os.getenv('PIPE_PIPELINEPATH')
PIPE_PROJECTPATH = os.getenv('PIPE_PROJECTPATH')

with open(os.path.join(PIPE_PIPELINEPATH, "PIPELINE_CONSTANTS.json"), "r") as read_file:
    PIPELINE_CONSTANTS = json.load(read_file)

SG_PROJECTNAME = PIPELINE_CONSTANTS['SG_PROJECTNAME'].encode('utf-8')


#################################################################################################

def main():


    if not pm.about(batch=1):
        import autosave_reminder.autosave_reminder
        import os
        import maya.cmds
        main_window = pm.language.melGlobals['gMainWindow']
        menu_obj = "PipelineToolsMenu"
        menu_label = SG_PROJECTNAME #"StuProVFX PipeTools"

        if pm.menu(menu_obj, label=menu_label, exists=True, parent=main_window):
            pm.deleteUI(pm.menu(menu_obj, e=True, deleteAllItems=True))
            
        custom_tools_menu = pm.menu(menu_obj, label=menu_label, parent=main_window, tearOff=False)

        icons_dir = os.path.join(os.path.dirname(__file__), "icons")

        # pm.menuItem(label="Render Submitter", command='import pyblish_main.load_pyblish; reload(pyblish_main.load_pyblish); os.environ["PYBLISH_FINAL_ACTION"]="Render"; pyblish_main.load_pyblish.load_pyblish()', image=icons_dir+"/send.png")
        # pm.menuItem(label="Render Checker", command='import render_checker.skRenderChecker; reload(render_checker.skRenderChecker); render_checker.skRenderChecker.main()', image=icons_dir+"/renderchecker.png")
        # pm.menuItem(label="Playblast", command='import pyblish_main.load_pyblish; reload(pyblish_main.load_pyblish); os.environ["PYBLISH_FINAL_ACTION"]="PlayBlast"; pyblish_main.load_pyblish.load_pyblish()', image=icons_dir+"/playblast.png")
        pm.menuItem(label="Playblast", command='import fancyPlayblast.fancyPlayblast; reload(fancyPlayblast.fancyPlayblast); fancyPlayblast.fancyPlayblast.showFancyPlayblast()', image=icons_dir+"/playblast.png")
        pm.menuItem(label="Render Sequence",
                    command='import fastRenderSequence.fastRenderSequence; reload(fastRenderSequence.fastRenderSequence); fastRenderSequence.fastRenderSequence.showRenderer()')
        pm.menuItem(label="Export All as FBX",
                    command='import fbx_export.funFbxExport; reload(fbx_export.funFbxExport); fbx_export.funFbxExport.showRenderer()')
        pm.menuItem(label="Export Selected as FBX",
                    command='import fbx_export.funFbxSelectedExport; reload(fbx_export.funFbxSelectedExport); fbx_export.funFbxSelectedExport.showRenderer()')

        pm.menuItem(label="Publish", command='import pyblish_main.load_pyblish; reload(pyblish_main.load_pyblish); os.environ["PYBLISH_FINAL_ACTION"]="Publish"; pyblish_main.load_pyblish.load_pyblish()', image=icons_dir+"/pyblish.png")
        pm.menuItem(label="Color Assigner", command='import color_assigner.skColorAssigner; reload(color_assigner.skColorAssigner); color_assigner.skColorAssigner.main()', image=icons_dir+"/colorchecker.png")
        pm.menuItem(label="Load Shelf...", command='import shelves.build_shelves; reload(shelves.build_shelves); shelves.build_shelves.main()', image=icons_dir+"/shelf.png")
        # pm.menuItem(label="Load Plugins...", command='import nodes.load_plugins; reload(nodes.load_plugins); nodes.load_plugins.main()', image=icons_dir+"/plugin.png")
        pm.menuItem(label="Autosave Reminder", command='import autosave_reminder.autosave_reminder; reload(autosave_reminder.autosave_reminder); autosave_reminder.autosave_reminder.main()', image=icons_dir+"/save.png")
        pm.setParent('..', menu=True)
        pm.menuItem(label="MOD", subMenu=True)
        pm.menuItem(label="Quad Fill Hole",
                    command='import QuadFillHole.GN_QuadFillHole; reload(QuadFillHole.GN_QuadFillHole); QuadFillHole.GN_QuadFillHole.QFH_Window()',
                    image=icons_dir + "/GN_QuadFillHole.png")
        pm.menuItem(label = "Turntable", command = "import shelves.scripts.turntable; reload(shelves.scripts.turntable); shelves.scripts.turntable.TurntableWindow()", stp = "python")
        pm.setParent('..', menu=True)
        pm.menuItem(label="LDV", subMenu=True)
        pm.menuItem(label="Load Lookdev",
                    command='import utils.load_lookdev; reload(utils.load_lookdev); utils.load_lookdev.main()',
                    image=icons_dir + "/lookdev.png")
        pm.menuItem(label="Delete Unused Textures", command='import utils.delete_unused_textures; reload(utils.delete_unused_textures); utils.delete_unused_textures.main()')
        pm.menuItem(label="Refresh Textures",
                    command='import utils.refresh_textures; reload(utils.refresh_textures); utils.refresh_textures.refresh_textures()')
        pm.setParent('..', menu=True)
        pm.menuItem(label="RIG", subMenu=True)
        pm.menuItem(label = "JbRigIcons", command = "import shelves.scripts.JbRigIcons.JbRigIcons as jb; jb.JbRigIcons().create()", stp = "python")
        pm.menuItem(label = "JbHashRename", command = "import shelves.scripts.jbHashRename; reload(shelves.scripts.jbHashRename); shelves.scripts.jbHashRename.jbHashRename().hashRenameSelection()", stp = "python")
        pm.setParent('..', menu=True)
        pm.menuItem(label="ANIM", subMenu=True)
        studio_library_path = os.path.join(os.getenv("PIPE_PROJECTPATH"), "_STUDIOLIBRARY")
        pm.menuItem(label="Studio Library",
                       command="import studiolibrary; reload(studiolibrary); studiolibrary.main(name='Shared', path='{path}')".format(path=studio_library_path),
                       stp="python", annotation="Studio Library.",
                       image=os.path.abspath(os.path.dirname(__file__) + "/../studiolibrary/resource/icons/icon.png"))
        pm.menuItem(label="aTools",
                    command="import aTools.setup; aTools.setup.install()",)
        animschoolpicker_path = os.path.join(os.getenv('PIPE_PIPELINEPATH'), 'maya\python\AnimSchoolPicker2020', 'AnimSchoolPicker.mll')
        asp_path_corrected = animschoolpicker_path.replace("\\", "\\\\")
        pm.menuItem(label="Anim School Picker",
                    command="maya.cmds.loadPlugin('" + asp_path_corrected + "'); maya.cmds.AnimSchoolPicker()",
                    image=os.path.abspath(os.path.dirname(__file__) + "/../AnimSchoolPicker2020/AnimSchoolLogoIcon.png"))
        pm.setParent('..', menu=True)
