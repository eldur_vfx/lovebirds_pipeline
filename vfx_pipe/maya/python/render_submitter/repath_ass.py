from arnold import *
import os

def repath(search_dir, out_dir, out_name):

    file_list = os.listdir(search_dir)

    for ass_file in file_list:

        if ".ass" in ass_file:

            AiBegin()
            
            ass_file = os.path.join(search_dir, ass_file)
            
            AiASSLoad(ass_file, AI_NODE_ALL)
                
            iter = AiUniverseGetNodeIterator(AI_NODE_DRIVER)
            
            while not AiNodeIteratorFinished(iter):
                node = AiNodeIteratorGetNext(iter)
                        
                if AiNodeIs(node, "driver_exr"):
                                    
                    filename = AiNodeGetStr(node, "filename")
                                                            
                    file_version = filename.split(".")[-2]
                    
                    file_ext = filename.split(".")[-1]
                    
                    file_full_name = ".".join([out_name, file_version, file_ext])
                    
                    file_path = "/".join([out_dir, file_full_name])
                                
                    AiNodeSetStr(node, "filename", file_path)

            AiNodeIteratorDestroy(iter)
            AiASSWrite(ass_file, AI_NODE_ALL, False, False)
            
            AiEnd()
                
        else:
            print("file does not exist")
