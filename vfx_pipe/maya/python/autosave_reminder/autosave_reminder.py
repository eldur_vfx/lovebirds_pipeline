from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui

import threading

from shiboken2 import wrapInstance

import maya.OpenMayaUI as omui

import pymel.core as pm
import os

try:
    import sgtk
    sgeng = sgtk.platform.current_engine()
except Exception as e:
    sgeng = None

def maya_main_window():
    main_window_ptr = omui.MQtUtil.mainWindow()
    return wrapInstance(int(main_window_ptr), QtWidgets.QWidget)

class Autosave_Reminder(QtWidgets.QDialog):

    def __init__(self, parent=maya_main_window()):
        super(Autosave_Reminder, self).__init__(parent)

        self.setWindowTitle("Autosave Reminder")
        self.setMinimumWidth(280)

        # Remove the ? from the dialog on Windows
        self.setWindowFlags( self.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)
        self.setWindowFlags( self.windowFlags() ^ QtCore.Qt.WindowStaysOnTopHint)

        self.create_widgets()
        self.create_layouts()
        self.connect_widgets()

    def create_widgets(self):

        icon = QtGui.QIcon(os.path.join(os.path.dirname(__file__), "sg_logo.png"))
        self.setWindowIcon(icon)
        
        self.message_label = QtWidgets.QLabel("Autosave reminder:\n\nSave your scene now?\n")
        self.message_label.setAlignment(QtCore.Qt.AlignCenter)
        self.message_label.setStyleSheet("font: bold 14px")
        self.cancel_button = QtWidgets.QPushButton("Not Now")
        self.save_button = QtWidgets.QPushButton("File Save...")
        self.enable_checkbox = QtWidgets.QCheckBox("Enable")
        self.enable_checkbox.setChecked(True)
        self.time_step_label = QtWidgets.QLabel("Time Step:")
        self.time_step_lineedit = QtWidgets.QLineEdit("15")
        self.time_step_lineedit.setValidator(QtGui.QDoubleValidator())
        self.time_step_lineedit.setMaximumWidth(40)
        self.minutes_label = QtWidgets.QLabel("min\t")

        self.setStyleSheet(r"QPushButton:hover{background: #278cbc; color: black}")

    def create_layouts(self):

        settings_layout = QtWidgets.QHBoxLayout()
        settings_layout.addWidget(self.time_step_label)
        settings_layout.addWidget(self.time_step_lineedit)
        settings_layout.addWidget(self.minutes_label)
        settings_layout.addWidget(self.enable_checkbox)
        settings_layout.addStretch()

        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addWidget(self.save_button)
        button_layout.addWidget(self.cancel_button)
        
        main_layout = QtWidgets.QVBoxLayout(self)
        main_layout.addWidget(self.message_label)
        main_layout.addLayout(settings_layout)
        main_layout.addLayout(button_layout)

    def connect_widgets(self):
        self.cancel_button.clicked.connect(self.close_window)
        self.save_button.clicked.connect(self.save_scene)

    def close_window(self):
        self.hide()
        self.start_timer()

    def start_timer(self):
        if self.enable_checkbox.isChecked():
            time_step = float(self.time_step_lineedit.text())
            timer = threading.Timer(60*time_step, self.show)
            timer.start()
        else:
            self.close()

    def save_scene(self):
        if sgeng is not None:
            sgeng.commands["File Save..."]["callback"]()
        else:
            pm.mel.eval("incrementAndSaveScene 0")

        self.start_timer()
        self.hide()

def main():
    d = Autosave_Reminder()
    d.show()

'''
import autosave_reminder.autosave_reminder
reload(autosave_reminder.autosave_reminder)
autosave_reminder.autosave_reminder.main()
'''