import pyblish.api
import re
import pymel.core as pm

try:
    import sgtk

    eng = sgtk.platform.current_engine()
except Exception as e:
    print(e)
    eng = None

if eng is not None:
    entity = str(eng.context).split(" ")[-1]
    entitiy_step = eng.context.step["name"]
    entitiy_type = eng.context.entity["type"]
else:
    entity = ""

shader_name = entity + "_aiStandardSurface"
geo_grp_name = entity + '_GEO_GRP'

pyblish_context = None


class CollectSceneShaders(pyblish.api.ContextPlugin):
    order = pyblish.api.CollectorOrder  # <-- This is new
    name = "Scene Shaders"

    def process(self, context):
        global pyblish_context
        pyblish_context = context
        if entitiy_type == "Asset" and entitiy_step == "texturing":
            material_list = self.get_materials_in_scene()
            found_shader = False
            for shader in material_list:
                if shader.name() == shader_name:
                    context.create_instance(name=shader_name, family="shader", icon="bolt")
                    found_shader = True

            assert found_shader, "Could not find shader that fits the name: {}".format(shader_name)

    def get_materials_in_scene(self):
        material_list = []
        for shading_engine in pm.ls(type=pm.nt.ShadingEngine):
            if len(shading_engine):
                for material in shading_engine.surfaceShader.listConnections():
                    material_list.append(material)
        return material_list


class ValidateSceneShaders(pyblish.api.InstancePlugin):
    order = pyblish.api.ValidatorOrder
    families = ["shader"]
    optional = True

    def process(self, instance):
        pass
