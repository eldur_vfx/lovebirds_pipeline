# import pyblish.api
# import pymel.core as pm
# import re
#
#
# try:
#   import sgtk
#   eng = sgtk.platform.current_engine()
# except Exception as e:
#   eng = None
#
# if eng is not None:
#   entity = str(eng.context).split(" ")[-1]
#   entitiy_step = eng.context.step["name"]
#   entitiy_type = eng.context.entity["type"]
# else:
#   entitiy = ""
#
# pattern = entity + '_GEO_GRP'
# pattern_main = entity + "_MAIN_GRP"
#
# scene_transforms = pm.ls(type="transform")
#
# class SelectAffectedGeo(pyblish.api.Action):
#   label = "Select Affected Geo"
#   on = "failed"
#   families = ["geometry"]
#
#   def process(self, context, plugin):
#     for result in context.data["results"]:
#       if result["error"] is None:
#         continue
#       instance = result["instance"]
#
#       if instance.data["family"] == "geometry":
#
#         for mesh in instance.data["geo_with_history"]:
#           self.log.info("Selecting mesh with history: {}".format(mesh))
#           pm.select(mesh, add=True)
#
#         for mesh in instance.data["geo_with_nmv"]+instance.data["geo_with_nme"]+instance.data["geo_with_lf"]:
#           self.log.info("Selecting mesh with non-manifold geometry: {}".format(mesh))
#           pm.select(mesh, add=True)
#
#         for mesh in instance.data["geo_with_ie"]+instance.data["geo_with_iv"]:
#           self.log.info("Selecting mesh with invalid geometry: {}".format(mesh))
#           pm.select(mesh, add=True)
#
# class CollectSceneGeo(pyblish.api.ContextPlugin):
#
#   order = pyblish.api.CollectorOrder  # <-- This is new
#   name = "Scene Geo"
#
#   def process(self, context):
#     if entitiy_type == "Asset":
#       found_parent = False
#       for transform in scene_transforms:
#         if pattern == transform.stripNamespace():
#           entitiy_children = pm.listRelatives(transform, allDescendents =True)
#           context.create_instance(name=transform.name(), family="geometry", icon="cubes", valid=True, children=entitiy_children)
#           found_parent = True
#
#       if not found_parent:
#         if len(pm.ls(type="mesh")) != 0:
#           context.create_instance(name="Unassigned Geo", family="geometry", icon="cubes", valid=False)
#
#
# class ValidateSceneGeo(pyblish.api.InstancePlugin):
#
#   order = pyblish.api.ValidatorOrder
#   families = ["geometry"]
#   optional = True
#   actions = [SelectAffectedGeo]
#
#   def process(self, instance):
#     assert instance.data["valid"], "Couldn't find geo parent group. Should be called: {}".format(pattern)
#
#     scene_meshes = pm.ls(type="mesh")
#
#     errorneus_geos = []
#     geo_with_history = []
#     geo_with_nmv= []
#     geo_with_nme = []
#     geo_with_lf = []
#     geo_with_ie = []
#     geo_with_iv = []
#
#     for geo in scene_meshes:
#       geo_status = False
#       naming_exception = "lookdev"
#       if geo in instance.data["children"]:
#           geo_status = True
#       if geo_status is False and naming_exception not in geo.name():
#         errorneus_geos.append(geo.name())
#
#       if len([n for n in geo.history(il=1, pdo = True) if not isinstance(n, pm.nodetypes.GeometryFilter)]) != 0:
#         geo_with_history.append(geo.name())
#
#       if pm.polyInfo(geo, nme=True) is not None:
#         geo_with_nme.append(geo.name())
#       if pm.polyInfo(geo, nmv=True) is not None:
#         geo_with_nmv.append(geo.name())
#       if pm.polyInfo(geo, lf=True) is not None:
#         geo_with_lf.append(geo.name())
#       if pm.polyInfo(geo, iv=True) is not None:
#         geo_with_iv.append(geo.name())
#       if pm.polyInfo(geo, ie=True) is not None:
#         geo_with_ie.append(geo.name())
#
#     instance.data["errorneus_geos"] = errorneus_geos
#     instance.data["geo_with_history"] = geo_with_history
#     instance.data["geo_with_nmv"] = geo_with_nmv
#     instance.data["geo_with_nme"] = geo_with_nme
#     instance.data["geo_with_lf"] = geo_with_lf
#     instance.data["geo_with_ie"] = geo_with_ie
#     instance.data["geo_with_iv"] = geo_with_iv
#
#     assert len(errorneus_geos) == 0, "Following geos are not parented under GEO_GRP: {}".format(errorneus_geos)
#     assert len(geo_with_history) == 0, "Following geos have history: {}".format(geo_with_history)
#     assert len(geo_with_nmv) == 0, "Geo has non-manifold edges: {}".format(geo_with_nmv)
#     assert len(geo_with_nme) == 0, "Geo has non-manifold vertecies: {}".format(geo_with_nme)
#     assert len(geo_with_lf) == 0, "Geo has lamina faces: {}".format(geo_with_lf)
#     assert len(geo_with_ie) == 0, "Geo has invalid edges: {}".format(geo_with_ie)
#     assert len(geo_with_iv) == 0, "Geo has invalid vertecies: {}".format(geo_with_iv)
#
#     if entitiy_type == "Asset" and entitiy_step == "rigging":
#       main_grp = False
#       for parent in pm.listRelatives(instance.data["name"], parent=True):
#         if pattern_main in parent.name():
#           main_grp = True
#
#       assert main_grp, "{} should be in {}.".format(pattern, pattern_main)
