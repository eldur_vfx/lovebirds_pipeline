import pyblish.api
import pymel.core as pm

try:
    import sgtk

    eng = sgtk.platform.current_engine()
except Exception as e:
    eng = None

if eng is not None:
    entity = str(eng.context).split(" ")[-1]
    entitiy_step = eng.context.step["name"]
    entitiy_type = eng.context.entity["type"]
else:
    entitiy = ""


class CollectCurrentScene(pyblish.api.ContextPlugin):
    order = pyblish.api.CollectorOrder  # <-- This is new

    def process(self, context):
        name = "Current Scene"
        context.create_instance(name="This Scene", family="scene", icon="file-text-o")


class ValidateCurrentScene(pyblish.api.InstancePlugin):
    order = pyblish.api.ValidatorOrder
    optional = True
    families = ["scene"]

    def process(self, instance):
        self.find_unknown_nodes()
        self.find_local_textures()
        # self.delete_unused_nodes()
        self.find_illegal_lights()
        # self.set_unc_paths()

    def find_unknown_nodes(self):
        unknown_nodes = pm.ls(type="unknown")
        assert len(unknown_nodes) == 0, "Scene contains unknown nodes: {}".format(unknown_nodes)

    def find_local_textures(self):
        pass
    #     # textures
    #     textures = pm.ls(textures=True)
    #     local_textures = []
    #     for tex in textures:
    #         if str(pm.nodeType(tex)) == "file":
    #             file_path = tex.fileTextureName.get()
    #             if "Nextcloud" not in file_path:
    #                 local_textures.append(tex.name())
    #     assert len(local_textures) == 0, "Scene contains local files: {}".format(local_textures)

    def delete_unused_nodes(self):
        self.log.info("Deleting unused nodes.")
        pm.mel.eval("MLdeleteUnused;")

    def find_illegal_lights(self):
        light_grp_name = entity + "_LIGHT_GRP"
        if entitiy_type == "Shot" and entitiy_step == "lighting shading rendering":
            illegal_lights = []
            for item in pm.ls(type=pm.listNodeTypes("light")):
                if "ai" in str(item.type()) and len(pm.listRelatives(item, parent=True)) != 0:
                    assert light_grp_name in pm.ls(type="transform"), "{} does not exist.".format(light_grp_name)
                    if item not in pm.listRelatives(light_grp_name, allDescendents=True, shapes=True):
                        illegal_lights.append(str(item.name()))

            assert len(illegal_lights) == 0, "Following lights are not in {}: {}".format(light_grp_name,
                                                                                         ", ".join(illegal_lights))

    def set_unc_paths(self):
        self.log.info("Setting all paths to UNC (\\\\vfx-nas01\\vfx2\\).")
        files = pm.filePathEditor(q=1, listDirectories="")

        if files is not None:
            for item in files:
                file_items = pm.filePathEditor(q=1, listFiles=item, withAttribute=True)[1::2]
                for file_item in file_items:
                    if pm.nodeType(pm.PyNode(file_item)) != "reference":
                        print("Repathing item: {}".format(file_item))
                        pm.filePathEditor(file_item, replaceString=("Z:/", "//vfx-nas01/vfx2/"))

            ocio_path = pm.colorManagementPrefs(q=True, configFilePath=True)
            ocio_path = ocio_path.replace("Z:/", "//vfx-nas01/vfx2/")
            pm.colorManagementPrefs(e=True, configFilePath=ocio_path)
