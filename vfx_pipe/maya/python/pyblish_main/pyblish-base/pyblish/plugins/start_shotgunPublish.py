import pyblish.api
import pymel.core as pm
import re
import os

try:
    import sgtk

    eng = sgtk.platform.current_engine()
except Exception as e:
    eng = None


class PublishToShotgun(pyblish.api.ContextPlugin):
    order = pyblish.api.IntegratorOrder
    optional = True

    def process(self, context):

        if os.environ["PYBLISH_FINAL_ACTION"] == "Publish":
            if eng is not None:
                eng.commands["Publish..."]["callback"]()
        elif os.environ["PYBLISH_FINAL_ACTION"] == "Render":
            import render_submitter.skLayerCommit
            reload(render_submitter.skLayerCommit)
            eng.commands["File Save..."]["callback"]()
            render_submitter.skLayerCommit.main()
        elif os.environ["PYBLISH_FINAL_ACTION"] == "PlayBlast":
            import background_playblast.skPlayBlast
            reload(background_playblast.skPlayBlast)
            background_playblast.skPlayBlast.main()
