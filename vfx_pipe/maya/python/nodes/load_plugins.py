import pymel.core as pm
import os
import pymel.util as pu


node_list = [
    "skWheelRotationNode.py",
    "loadUNCReferenceCommand.py"
    "AnimSchoolPicker2020"
]

def main():
    pu.putEnv("MAYA_PLUG_IN_PATH", pu.getEnv("MAYA_PLUG_IN_PATH")+";"+os.path.dirname(__file__).replace("\\", "/"))
    for node_name in node_list:
        pm.loadPlugin(node_name, qt=True)