import sys
import maya.api.OpenMaya as OpenMaya
# ... additional imports here ...
import pymel.core as pm


##########################################################
# Callback Function
##########################################################
def callbackFunction(fileObject, retCode):
    import pymel.core as pm
    if pm.about(batch=True):
        # printing the original file path
        print("Original file path %s" % fileObject.rawFullName())
        print("repathing files #################")
        # create a variable for the new file path
        # maybe create some logic for that or even a condition whether to
        # manipulate the path or keep the existing one
        newFilePath = fileObject.rawFullName().replace("Z:/", "//vfx-nas01/vfx2/")

        # set the file path to be loaded if you want to modify it
        fileObject.setRawFullName(newFilePath)

        # here you can define whether the file should be loaded or
        # whether the reference should be skipped
        return True
    elif input() == 1:
        print("Not in batch mode - paths won't be changed.")
        # printing the original file path
        print("Original file path %s" % fileObject.rawFullName())
        print("repathing files #################")
        # create a variable for the new file path
        # maybe create some logic for that or even a condition whether to
        # manipulate the path or keep the existing one
        newFilePath = fileObject.rawFullName().replace("Z:/", "//vfx-nas01/vfx2/")

        # set the file path to be loaded if you want to modify it
        fileObject.setRawFullName(newFilePath)

        # here you can define whether the file should be loaded or
        # whether the reference should be skipped
        return True

    else:
        return True


##########################################################
# Plug-in 
##########################################################
class MyCommandClass(OpenMaya.MPxCommand):
    kPluginCmdName = 'repathUNC'

    callback_ids = []

    def __init__(self):
        """ Constructor. """
        OpenMaya.MPxCommand.__init__(self)

    @staticmethod
    def cmdCreator():
        """ Create an instance of our command. """
        return MyCommandClass()

    def doIt(self, args):
        import pymel.core as pm
        ''' Command execution. '''
        # Remove the following 'pass' keyword and replace it with 
        # the code you want to run.
        if pm.about(batch=True):
            print("running unc repath callback")

            self.callback_ids.append(
                OpenMaya.MSceneMessage.addCheckFileCallback(OpenMaya.MSceneMessage.kBeforeLoadReferenceCheck,
                                                            callbackFunction)
            )
            self.callback_ids.append(
                OpenMaya.MSceneMessage.addCheckFileCallback(OpenMaya.MSceneMessage.kBeforeCreateReferenceCheck,
                                                            callbackFunction)
            )
        else:
            print("not in batch mode, not running command")


##########################################################
# Plug-in initialization.
##########################################################

def maya_useNewAPI():
    """
    The presence of this function tells Maya that the plugin produces, and
    expects to be passed, objects created using the Maya Python API 2.0.
    """
    pass


def initializePlugin(mobject):
    import pymel.core as pm
    ''' Initialize the plug-in when Maya loads it. '''
    mplugin = OpenMaya.MFnPlugin(mobject)
    try:
        mplugin.registerCommand(MyCommandClass.kPluginCmdName,
                                MyCommandClass.cmdCreator)
    except:
        sys.stderr.write('Failed to register command: ' + MyCommandClass.kPluginCmdName)

    pm.mel.eval("repathUNC")


def uninitializePlugin(mobject):
    """ Uninitialize the plug-in when Maya un-loads it. """
    mplugin = OpenMaya.MFnPlugin(mobject)
    try:
        mplugin.deregisterCommand(MyCommandClass.kPluginCmdName)
    except:
        sys.stderr.write('Failed to unregister command: ' + MyCommandClass.kPluginCmdName)
