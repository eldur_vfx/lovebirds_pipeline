import pymel.core as pm
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui
from shiboken2 import wrapInstance
import maya.OpenMayaUI as omui

def maya_main_window():
    main_window_ptr = omui.MQtUtil.mainWindow()
    return wrapInstance(int(main_window_ptr), QtWidgets.QWidget)

class Color_Assigner(MayaQWidgetDockableMixin, QtWidgets.QDialog):

    def __init__(self, parent=maya_main_window()):
        super(Color_Assigner, self).__init__(parent)

        self.setWindowTitle("skColorAssigner")
        self.setFixedHeight(250)
        self.setFixedWidth(350)

        self.create_widgets()
        self.create_layouts()
        self.create_connections()

    def create_widgets(self):
        self.toggle_button = QtWidgets.QPushButton("Toggle")
        self.strip_button = QtWidgets.QPushButton("Strip")
        self.select_button = QtWidgets.QPushButton("Get Matching")
        self.cancel_button = QtWidgets.QPushButton("Cancel")

        self.color_dialog = QtWidgets.QColorDialog()

        self.mesh_checkbox = QtWidgets.QCheckBox("Mesh")
        self.mesh_checkbox.setChecked(True)
        self.outliner_checkbox = QtWidgets.QCheckBox("Outliner")
        self.outliner_checkbox.setChecked(True)
        self.wireframe_checkbox = QtWidgets.QCheckBox("Wireframe")

        self.setStyleSheet(r"QPushButton:hover{background: #278cbc; color: black} QGroupBox {font: bold 14px;}")

    def create_layouts(self):

        color_box = QtWidgets.QGroupBox("Assign Colors")
        color_layout = QtWidgets.QGridLayout()
        self.populate_color_layout(color_layout)
        color_box.setLayout(color_layout)

        checkbox_box = QtWidgets.QGroupBox()
        checkbox_layout = QtWidgets.QHBoxLayout()
        checkbox_layout.addWidget(self.mesh_checkbox)
        checkbox_layout.addWidget(self.outliner_checkbox)
        checkbox_layout.addWidget(self.wireframe_checkbox)
        checkbox_box.setLayout(checkbox_layout)
        checkbox_box.setFixedHeight(35)

        button_layout = QtWidgets.QHBoxLayout()
        button_layout.addWidget(self.toggle_button)
        button_layout.addWidget(self.strip_button)
        button_layout.addWidget(self.select_button)
        button_layout.addWidget(self.cancel_button)
        
        main_layout = QtWidgets.QVBoxLayout(self)
        main_layout.addWidget(color_box)
        main_layout.addWidget(checkbox_box)
        main_layout.addLayout(button_layout)

    def create_connections(self):
        self.cancel_button.clicked.connect(self.close)
        self.strip_button.clicked.connect(self.strip_colors)
        self.select_button.clicked.connect(self.select_matching)
        self.toggle_button.clicked.connect(self.toggle_colors)
    
    def populate_color_layout(self, layout):

        self.button_list = []

        self.color_dict = [
            ["purple", "#C51162"],
            ["red", "#FF1744"],
            ["orange", "#F9A825"],
            ["yellow", "#FDD835"],
            ["grass_green", "#76FF03"],
            ["green", "#00E676"],
            ["teal", "#64FFDA"],
            ["blue", "#2196F3"],
            ["pink", "#F06292"],
            ["violet", "#E040FB"],
            ["white", "#FFFFFF"],
            ["grey", "#B0BEC5"],
            ["black", "#000000"]
            
        ]
        width = 5
        height = 3

        k = 0
        for i in range(height):
            for j in range(width):
                if k < len(self.color_dict):
                    label = self.color_dict[k][0]
                    color = self.color_dict[k][1]
                    color_button = QtWidgets.QPushButton()
                    color_button.setStyleSheet("background-color: {};".format(color))
                    layout.addWidget(color_button, i, j)
                    self.button_list.append(color_button)
                    k += 1
        
        for button in self.button_list:
            button.clicked.connect(self.make_function(button))

    def make_function(self, button):
        def call_setter():
            color = button.styleSheet().split(" ")[-1]
            self.set_current_color(color)
        return call_setter

    def set_current_color(self, color):
        selection = pm.ls(sl=1)
        rgb = self.hex_to_rgb(color)
        rgb_tup = (rgb[0]/255.0, rgb[1]/255.0, rgb[2]/255.0)
        for obj in selection:
            obj_type = str(type(obj)).lower()
            if "mesh" in obj_type or "meshface" in obj_type or "transform" in obj_type:
                if self.mesh_checkbox.isChecked():
                    pm.polyColorPerVertex(obj, rgb=rgb_tup, cdo=True)
                if "meshface" in obj_type:
                    continue
                if self.wireframe_checkbox.isChecked():
                    obj.getShape().overrideEnabled.set(1)
                    obj.getShape().overrideRGBColors.set(True)
                    obj.getShape().overrideColorRGB.set(rgb_tup)
                if self.outliner_checkbox.isChecked():
                    obj.useOutlinerColor.set(True)
                    obj.outlinerColor.set(rgb_tup)

    def select_matching(self):
        selection = pm.ls(sl=1, type="transform")
        pm.select(clear=True)

        color_combinations = []

        selection_list = []

        for obj in selection:
            mesh_color_search = None
            wire_color_search = None
            outliner_color_search = None
            if self.mesh_checkbox.isChecked():
                mesh_color_search = pm.polyColorPerVertex(obj.vtx[0], q=1, rgb=1, cdo=1)
            if self.wireframe_checkbox.isChecked():
                wire_color_search = obj.getShape().overrideColorRGB.get()
            if self.outliner_checkbox.isChecked():
                outliner_color_search = obj.outlinerColor.get()

            color_combinations.append([mesh_color_search, wire_color_search, outliner_color_search])
            
            for obj in pm.ls(type="transform", visible=True):
                if pm.nodeType(obj.getShape()) == "mesh":
                    pm.select(obj)
                    pm.mel.ToggleDisplayColorsAttr()
                    pm.mel.ToggleDisplayColorsAttr()
                    pm.select(clear=True)
                    mesh_color = None
                    wire_color = None
                    outliner_color = None
                    if self.mesh_checkbox.isChecked():
                        try:
                            mesh_color = pm.polyColorPerVertex(obj.vtx[0], q=1, rgb=1)
                        except Exception as e:
                            pass
                    if self.wireframe_checkbox.isChecked():
                        wire_color = obj.getShape().overrideColorRGB.get()
                    if self.outliner_checkbox.isChecked():
                        outliner_color = obj.outlinerColor.get()

                    combination = [mesh_color, wire_color, outliner_color]
                    if combination in color_combinations:
                        selection_list.append(obj)

        pm.select(selection_list)

    def hex_to_rgb(self, value):
        value = value.strip(";").lstrip('#')
        hlen = len(value)
        return tuple(int(value[i:i+hlen/3], 16) for i in range(0, hlen, hlen/3))

    def toggle_colors(self):
        for obj in pm.ls(sl=1, type="transform"):
            if pm.nodeType(obj.getShape()) == "mesh":
                if self.mesh_checkbox.isChecked():
                    pm.mel.ToggleDisplayColorsAttr()
                if self.outliner_checkbox.isChecked():
                    if obj.useOutlinerColor.get():
                        obj.useOutlinerColor.set(False)
                    else:
                        obj.useOutlinerColor.set(True)

    def strip_colors(self):
        for obj in pm.ls(sl=1, type="transform"):
            if pm.nodeType(obj.getShape()) == "mesh":
                if self.mesh_checkbox.isChecked():
                    pm.polyColorPerVertex(obj, rgb=(0.216,0.216,0.216), cdo=1)
                if self.wireframe_checkbox.isChecked():
                    obj.getShape().overrideEnabled.set(1)
                    obj.getShape().overrideRGBColors.set(True)
                    obj.getShape().overrideColorRGB.set(0.0, 0.001, 0.117)
                if self.outliner_checkbox.isChecked():
                    obj.outlinerColor.set(0, 0, 0)
                    obj.useOutlinerColor.set(False)

def main():
    d = Color_Assigner()
    d.show(dockable=True)

if __name__ == "__main__":
    main()