'''##########################################  GN Quad Fill Hole #############################################
	 ___________________________________
	|									|
	|	Author: Gabriel Nadeau			|
	|	URL: www.gabrielnadeau.com		|
	|	Version: 3.02					|
	|	Release Date: 27-11-2020		|
	|___________________________________|
	
	
	INSTALLATION:
	- Place the file "GN_QuadFillHole.py" into your maya script directory:
		C:\Users\YourUsername\Documents\maya\MayaVersion\scripts
	- Make sure to exit & re-open Maya before running the script.

	HOW TO USE:
	- In Maya, select a vertex located on the edge border of a polygon mesh.
	- In the Python command line or script editor, type the following command and press enter:
	>> You can you either run the script with the current options:
		import GN_QuadFillHole
		GN_QuadFillHole.QFH_Window(optionBox=False)
	>> Or you can open the script option box to change the options and run the script from there:
		import GN_QuadFillHole
		GN_QuadFillHole.QFH_Window(optionBox=True)

	OPTIONS:
	>> Alternate action:
	  Description: This option box lets you choose what to do in situations where a Quad Fill Hole is not possible.
	- None: No alternate action.
	- Fill Hole: Fills the hole with a single face.
	- Close Hole: Fills the hole with multiple faces merged at the center.

	>> Add inset / Inset slider:
	- If you check the "Add inset" checkbox, an inset will be added to the filled hole and the "Inset" slider will be unlocked.
	- The "Inset" slider allows you to control the size of the inset.

	>> Offset:
	- This option box allows you to add a positive/negative offset to the Quad Fill Hole in specific situations where it could help get a better result.

	>> Force offset:
	- If you check this box, an offset will be applied to the Quad Fill Hole no matter what, which can sometimes help get a better result.

	TIPS:
	- You can add a button to your shelf with the command above or create a runtime command in the Hotkey Editor and assign a hotkey to it.
	- Feel free to use the icon "GN_QuadFillHole.png" provided with this script for your shelf button.

	NOTE:
	-  This script was created in Maya 2019 and might not work in earlier versions.
	
###########################################################################################################'''

import maya.cmds as cmds
import maya.api.OpenMaya as om
from maya.mel import eval as melEval
from collections import OrderedDict
from functools import partial
import math

def GN_GetSelection(*args, **kwargs):
	# Check if there is any args
	if len(args) > 0:
		selection, objects = [], []
		# Get selection & objects from args
		for arg in args:
			selection.extend(arg)
			objects.extend(arg)
	else:
		# Get selection
		selection = cmds.ls(sl=True, l=True)
		objects = list(selection)
		
		# Get highlighted objects
		if kwargs["highlight"] is True:
			objects.extend(cmds.ls(hl=True, l=True))
	
	# Get shapes
	if kwargs["type"] == "mesh":
		shapes = cmds.ls(objects, l=True, dag=True, ni=True, o=True, typ="mesh")
	elif kwargs["type"] == "geometry" or kwargs["type"] == "geo":
		shapes = cmds.ls(objects, l=True, dag=True, ni=True, o=True, g=True)
	elif kwargs["type"] == "nurbsCurve" or kwargs["type"] == "curve":
		shapes = cmds.ls(objects, l=True, dag=True, ni=True, typ="nurbsCurve")
	elif kwargs["type"] == "transform":
		shapes = cmds.ls(objects, l=True, dag=True, ni=True, o=True, typ="transform")
	else:
		shapes = cmds.ls(objects, l=True, dag=True, ni=True, o=True)
		
	# Get objects
	objects = cmds.listRelatives(shapes, f=True, p=True, ni=True, typ="transform")
	
	# Remove intermediate objects
	objects = cmds.ls(objects, l=True, ni=True)
	
	# Remove duplicates objects
	objects = list(OrderedDict.fromkeys(objects))
	
	return selection, shapes, objects

def GN_GetComponentType(*args, **kwargs):
	# Create default args
	selection = None
	
	# Load arguments (override default args)
	for i, arg in enumerate(args):
		if i == 0:
			selection = arg
	
	# Load keyworded arguments (override *args)
	for key, value in kwargs.iteritems():
		if key == "selection" or key == "sel":
			selection = value
	
	selList = om.MSelectionList()
	
	if selection is not None:
		for each in selection:
			try:
				selList.add(each)
			except:
				pass
	else:
		om.MGlobal_getActiveSelectionList(selList)
	
	filterDict = OrderedDict([("v", om.MFn.kMeshVertComponent), ("eg", om.MFn.kMeshEdgeComponent), ("fc", om.MFn.kMeshPolygonComponent), ("puv", om.MFn.kMeshMapComponent), ("pvf", om.MFn.kMeshVtxFaceComponent), ("cv", om.MFn.kCurveCVComponent)])
	
	for key, filter in filterDict.iteritems():
		iter = om.MItSelectionList(selList, filter)
		try:
			if iter.hasComponents() is True:
				return key
		except:
			pass
	
	return None

def GN_CalculateDistance(point1, point2):
    pos1 = cmds.xform(point1, q=True, ws=True, t=True)
    pos2 = cmds.xform(point2, q=True, ws=True, t=True)
    dist = om.MVector(pos2[0]-pos1[0], pos2[1]-pos1[1], pos2[2]-pos1[2])
    return dist.length()

def equivalentTol(a, b, tol):
    return abs(a - b) <= tol

def GN_EdgeLength(edges):
	edgeLength = 0
	for edge in edges:
		vertices = cmds.ls(cmds.polyListComponentConversion(edge, fe=True, tv=True), l=True, fl=True)
		edgeLength += GN_CalculateDistance(vertices[0], vertices[1])

	return edgeLength

def GN_VertAvg(vertList):
	count = len(vertList)
	sums = om.MVector()

	for vertex in vertList:
		pos = cmds.pointPosition(vertex, w=True)
		posVector = om.MVector(pos[0], pos[1], pos[2])
		sums += posVector
	
	avgPos = (sums / count)
	return avgPos

def GN_ConvertToBorderEdges(components):
	edges = cmds.polyListComponentConversion(components, te=True)
	edgeList = om.MSelectionList()
	
	for edge in edges:
		edgeList.add(edge)
	
	edges = []
	dagPath, component = edgeList.getComponent(0)
	edge_ids = om.MFnSingleIndexedComponent(component).getElements()
	edge_iter = om.MItMeshEdge(dagPath, component)
	
	# Remove shape from path
	if not dagPath.hasFn(om.MFn.kTransform):
		dagPath.pop()
	objectName = dagPath.fullPathName()
	
	for edge_id in edge_ids:
		edge_iter.setIndex(edge_id)
		if edge_iter.onBoundary():
			edge_string = "{0}.e[{1}]".format(objectName, edge_id)
			edges.append(edge_string)
		
	return edges

def GN_ConnectedVertices(vertex):
	conn = cmds.ls(cmds.polyListComponentConversion(vertex, te=True), l=True)
	conn = cmds.ls(cmds.polyListComponentConversion(conn, fe=True, tv=True), l=True, fl=True)
	return conn

def GN_ConnectedBorderEdges(edge):
	conn = cmds.ls(cmds.polyListComponentConversion(edge, tv=True), l=True)
	conn = GN_ConvertToBorderEdges(conn)
	return conn

def GN_ConnectedUvs(uv):
	conn = cmds.ls(cmds.polyListComponentConversion(uv, te=True), l=True)
	conn = cmds.ls(cmds.polyListComponentConversion(conn, fe=True, tuv=True), l=True, fl=True)
	return conn

def GN_GetBorderVertices(object, components):
	vertices = []
	edges = GN_ConvertToBorderEdges(components)
	if len(edges):
		edgeNum = int(edges[0].split("[")[-1][:-1])
		edgeBorder = cmds.polySelect(object, eb=edgeNum, ns=True, ass=True)
		vertices = cmds.ls(cmds.polyListComponentConversion(edgeBorder, fe=True, tv=True), l=True, fl=True)
	
	return vertices

def GN_GetEdgeBorderPathUv(start, end):
	edgeBorderPathUv = []
	startEdges = GN_ConvertToBorderEdges(start)
	endEdges = GN_ConvertToBorderEdges(end)
	
	if set(startEdges).intersection(endEdges):
		edgeBorderPathUv = [start, end]
	else:
		for edge in startEdges:
			traverseEdges = startEdges[:]
			traverseUvs = [start]
			
			while end not in traverseUvs:
				edges = GN_ConnectedBorderEdges(edge)
				
				for conn in edges:
					if conn not in traverseEdges:
							edge = conn
							traverseEdges.append(conn)
							
							uvs = GN_ConnectedUvs(conn)
							for uv in uvs:
								if uv not in traverseUvs:
									traverseUvs.append(uv)
			
			if not edgeBorderPathUv or len(traverseUvs) < len(edgeBorderPathUv):
				edgeBorderPathUv = traverseUvs
	
	return edgeBorderPathUv

def GN_VertexTraverse(start, listToWalk, count):
	traverse = [start]
	
	for x in xrange(count):
		neighbours = GN_ConnectedVertices(start)
		
		for n in neighbours:
			if n in listToWalk and n not in traverse:
				start = n
				traverse.append(n)
	
	return traverse[-1]

def GN_UvTraverse(start, listToWalk):
	traverse = [start]
	
	for x in xrange(len(listToWalk)):
		neighbours = GN_ConnectedUvs(start)
		
		for n in neighbours:
			if n in listToWalk and n not in traverse:
					start = n
					traverse.append(n)
	
	return traverse

def GN_RelinkComponents(object, components):
	relinkList = []
	
	for item in components:
		item = item.split(".")[-1]
		relinkList.append(object + "." + item)
		
	return relinkList

def GN_CreateUVs(faces):
	cmds.select(faces, r=True)
	cmds.polyPlanarProjection(ch=False, ibd=True, kir=True, md="b")
	cmds.select(faces, d=True)

def GN_CalculateAngle(uv0, uv1):
	pointA = cmds.polyEditUV(uv0, q=True)
	pointB = cmds.polyEditUV(uv1, q=True)
	
	# Find left point (results can be both positive and negative)
	if pointA[0] >= pointB[0]:
		uDist = pointA[0] - pointB[0]
		vDist = pointA[1] - pointB[1]
	else:
		uDist = pointB[0] - pointA[0]
		vDist = pointB[1] - pointA[1]
	
	# Calculate arc tangent in degrees
	angle = math.degrees(math.atan2(vDist, uDist))
	
	return angle

def GN_OrientUvs(uv0, uv1):
	# Calculate angle
	angle = GN_CalculateAngle(uv0, uv1)
	
	# Reduce arctan angle to 4 decimals only
	angle = round(angle, 4)
	
	# Determine rotation type based on arc tangent
	# Arctan range is -90 deg to +90 deg    
	if any([angle == 0.0000, angle == 90.0000, angle == -90.0000]):
		# Type 0 - No rotation
		angle = 0
		invertVal = 0
	elif angle >= -44.9999 and angle <= 44.9999:
		# Type A - Invert arctan
		invertVal = 1
	elif angle >= 45.0001 and angle <= 89.9999:
		# Type B - Subtract arctan from 90
		angle = 90 - angle
		invertVal = 0
	elif angle <= -45.0001 and angle >= -89.9999:
		# Type C - Add 45 to arctan and invert
		angle = 90 + angle
		invertVal = 1
	else:
		# Type D - 45 degrees vector
		angle = 45
		invertVal = 0
	
	if invertVal == 1:
		angle = -angle
	
	uvShell = cmds.polyListComponentConversion(uv1, tuv=True, uvs=True)
	uvBox = cmds.polyEvaluate(uvShell, bc2=True)
	uvBox = [y for x in uvBox for y in x]
	pivotU = 0.5 * (uvBox[0] + uvBox[1])
	pivotV = 0.5 * (uvBox[2] + uvBox[3])
	cmds.polyEditUV(uvShell, pu=pivotU, pv=pivotV, a=angle)

def GN_DeleteOtherFaces(object, faces):
	shells = cmds.polyEvaluate(object, s=True)
	cmds.polyChipOff(faces, ch=False, dup=False)
	separate = cmds.ls(cmds.polySeparate(object, ch=False, rs=True), l=True)
	
	parent = object
	if shells > 1:
		object = separate[:-1]
		separate = separate[-1]
	else:
		object = separate[-1]
		separate = separate[:-1]
	
	cmds.delete(separate)
	
	object = cmds.ls(cmds.parent(object, w=True), l=True)[0]
	cmds.delete(parent)
	
	return object

def GN_GetCornerUvs(object):
	uvs = cmds.ls((object + ".map[*]"), l=True, fl=True)
	uvValues = [0, 0, 1, 0]
	uvComparison = [1, 0, 1, 0]
	cornerUvs = ["", "", "", ""]
	
	# Get the UV values of the corner uvs
	for uv in uvs:
		uvValue = cmds.polyEditUV(uv, q=True)
		uvAvg = (uvValue[0] + uvValue[1]) / 2
		uvAvg0 = uvValue[1] - uvValue[0]
		uvAvg3 = uvValue[0] - uvValue[1]
		
		# uv0 (Top Left)
		if uvAvg0 > uvValues[0]:
			uvValues[0] = uvAvg0
		
		# uv1 (Top Right)
		if uvAvg > uvValues[1]:
			uvValues[1] = uvAvg
		
		# uv2 (Bottom Left)
		if uvAvg < uvValues[2]:
			uvValues[2] = uvAvg
		
		# uv3 (Bottom Right)
		if uvAvg3 > uvValues[3]:
			uvValues[3] = uvAvg3
	
	# Get the corner UVs
	for uv in uvs:
		uvValue = cmds.polyEditUV(uv, q=True)
		uvAvg = (uvValue[0] + uvValue[1]) / 2
		uvAvg0 = uvValue[1] - uvValue[0]
		uvAvg3 = uvValue[0] - uvValue[1]
		
		# uv0 (Top Left)
		if equivalentTol(uvAvg0, uvValues[0], 0.0001) and uvValue[0] < uvComparison[0]:
			uvComparison[0] = uvValue[0]
			cornerUvs[0] = uv
		
		# uv1 (Top Right)
		if equivalentTol(uvAvg, uvValues[1], 0.0001) and uvValue[0] > uvComparison[1]:
			uvComparison[1] = uvValue[0]
			cornerUvs[1] = uv
		
		# uv2 (Bottom Left)
		if equivalentTol(uvAvg, uvValues[2], 0.0001) and uvValue[0] < uvComparison[2]:
			uvComparison[2] = uvValue[0]
			cornerUvs[2] = uv
		
		# uv3 (Bottom Right)
		if equivalentTol(uvAvg3, uvValues[3], 0.0001) and uvValue[0] > uvComparison[3]:
			uvComparison[3] = uvValue[0]
			cornerUvs[3] = uv
	
	# Remove duplicates
	cornerUvs = list(OrderedDict.fromkeys(cornerUvs))
	
	return cornerUvs

def GN_InBetweenUvs(object, cornerUvs, position):
	if position == "left":
		uvPair = [cornerUvs[2], cornerUvs[0]]
		
	elif position == "top":
		uvPair = [cornerUvs[0], cornerUvs[1]]
		
	elif position == "right":
		uvPair = [cornerUvs[3], cornerUvs[1]]
	
	elif position == "bottom":
		uvPair = [cornerUvs[2], cornerUvs[3]]
	
	uvs = GN_GetEdgeBorderPathUv(uvPair[0], uvPair[1])
	uvs = GN_UvTraverse(uvPair[0], uvs) # Re-order UVs
	
	return uvs

def GN_MoveUv(uvList, direction):
	uvs = uvList[1:-1] # Remove corner UVs from list
	
	for uv in uvs:
		uvValue = cmds.polyEditUV(uv, q=True)
		if direction == "left":
			cmds.polyEditUV(uv, r=False, u=0, v=uvValue[1])
		if direction == "top":
			cmds.polyEditUV(uv, r=False, u=uvValue[0], v=1)
		if direction == "right":
			cmds.polyEditUV(uv, r=False, u=1, v=uvValue[1])
		if direction == "bottom":
			cmds.polyEditUV(uv, r=False, u=uvValue[0], v=0)

def GN_AvgUvDistance(uvList, direction, coordinate):
	uvs = uvList[1:-1] # Remove corner UVs from list
	
	# Calculate average distance for each UVs
	for i in xrange(len(uvs)):
		value = (float(1) / (len(uvs) + 1)) * (i + 1)
		if direction == "u":
			cmds.polyEditUV(uvs[i], r=False, u=value, v=coordinate)
		if direction == "v":
			cmds.polyEditUV(uvs[i], r=False, u=coordinate, v=value)

def GN_GetPointAtUV(object, uvValues):
	faces = object + ".f[*]"
	faceList = om.MSelectionList().add(faces)
	
	dagPath, component = faceList.getComponent(0)
	fnMesh = om.MFnMesh(dagPath)
	ids = om.MFnSingleIndexedComponent(component).getElements()
	
	for id in ids:
		try:
			point = fnMesh.getPointAtUV(id, uvValues[0], uvValues[1], om.MSpace.kWorld)
			break
		except:
			pass
	
	return point

def GN_Combine(object, objectList):
	# Get object short name & parent
	shortName = object.split("|")[-1]
	parent = cmds.ls(cmds.listRelatives(object, f=True, p=True), l=True)
	
	# Combine objects
	combine = cmds.polyUnite(object, objectList)[0]
	
	# Match transforms
	cmds.parent(combine, object)
	cmds.makeIdentity(combine, a=True, t=True, r=True, s=True, n=False, pn=True)
	
	# Match pivot
	cmds.matchTransform(combine, object, piv=True)

	# Reparent object
	if len(parent):
		cmds.parent(combine, parent[0])
	else:
		cmds.parent(combine, w=True)
	
	# Delete history
	cmds.delete(combine, ch=True)
	
	# Delete empty group
	if cmds.objExists(object) is True:
	    cmds.delete(object)
	
	# Rename combined object
	object = cmds.rename(combine, shortName)

	# Merge vertices
	cmds.polyMergeVertex(object, ch=False, d=0.0001)
	
	# Add object to isolate mode if acivated
	currentPanel = cmds.getPanel(wf=True)
	if cmds.getPanel(to=currentPanel) != "modelPanel":
	    currentPanel = "modelPanel4"
	if cmds.isolateSelect(currentPanel, q=True, s=True):
	    cmds.isolateSelect(currentPanel, ado=object)

def GN_RestoreSelection(selection, selectType, objects):
	cmds.select(objects, r=True)
	cmds.selectMode(co=True), cmds.selectMode(o=True)
	
	if selectType != None:
		melEval("selectMode -co; selectType -"+selectType+" 1;")
		cmds.select(selection, r=True)

def GN_FillHole(object, edges):
	# Fill Hole
	cmds.select(edges, r=True)
	cmds.polyCloseBorder(edges)
	faceCount = cmds.polyEvaluate(object, f=True)
	face = object + ".f[" + str((faceCount - 1)) + "]"
	cmds.select(edges, d=True)
	cmds.delete(object, ch=True)
	
	# Return faces
	return face

def GN_CloseHole(object, edges):
	# Extrude edges and merge to center
	faces = []
	
	cmds.select(edges, r=True)
	cmds.polyExtrudeEdge(edges)
	extrudeEdges = cmds.ls(sl=True, l=True)
	vertices = cmds.ls(cmds.polyListComponentConversion(extrudeEdges, fe=True, tv=True), l=True, fl=True)
	faces = cmds.polyListComponentConversion(vertices, fv=True, tf=True)
	pos = GN_VertAvg(vertices)
	cmds.move(pos.x, pos.y, pos.z, vertices, ws=True)
	cmds.polyMergeVertex(vertices, d=0.0001)
	extrudeEdges = cmds.polyListComponentConversion(faces, ff=True, te=True, bo=True)
	cmds.polySoftEdge(vertices[0], a=180)
	cmds.delete(object, ch=True)
	
	# Return faces
	return faces

def GN_AlternateFillHole(object, edges, msg):
	alternateAction = cmds.optionVar(q="QFH_AlternateAction")
	forceAlternateAction = cmds.optionVar(q="QFH_ForceAlternateAction")
	faces = []
	if alternateAction == 1:
		msg = "// Error: " + msg
	
	elif alternateAction == 2:
		faces.append(GN_FillHole(object, edges))
		msg = "// Warning: " + msg + " Performing Fill Hole instead."
	
	elif alternateAction == 3:
		faces = GN_CloseHole(object, edges)
		msg = "// Warning: " + msg + " Performing Close Hole instead."
	
	if forceAlternateAction:
		msg = "// Result: GN_QuadFillHole"
	
	cmds.evalDeferred('print("'+msg+'\\n"),')
	
	return faces

def GN_QuadFillHoleUndo():
	redo = cmds.undoInfo(q=True, rn=True)
	if "GN_QuadFillHole" in redo:
		ds = cmds.displaySmoothness(q=True, po=True)[0]
		cmds.displaySmoothness(po=ds)
		print("Undo: GN_QuadFillHole\n"),

def GN_QuadFillHoleRedo():
	undo = cmds.undoInfo(q=True, un=True)
	if "GN_QuadFillHole" in undo:
		print("Redo: GN_QuadFillHole\n"),

def GN_QuadFillHole():
	# Open undo chunk
	cmds.undoInfo(ock=True, cn="GN_QuadFillHole")
	
	# Get option variables
	alternateAction = cmds.optionVar(q="QFH_AlternateAction")
	forceAlternateAction = cmds.optionVar(q="QFH_ForceAlternateAction")
	inset = cmds.optionVar(q="QFH_Inset")
	insetSlider = cmds.optionVar(q="QFH_InsetSlider")
	offset = cmds.optionVar(q="QFH_Offset")
	forceOffset = cmds.optionVar(q="QFH_ForceOffset")
	
	# Get selection
	selection, shapes, objects = GN_GetSelection(highlight=False, type="mesh")
	selectType = GN_GetComponentType(selection)
	
	# Check that something is selected
	if not len(selection):
		print("// Error: Nothing selected!\n"),
		return
	
	# Check that a single object is selected
	if len(objects) > 1:
		print("// Error: More than one object selected!\n"),
		return
	
	# Get border vertices
	object, shape = objects[0], shapes[0]
	vtxPair = [cmds.ls(cmds.polyListComponentConversion(selection, tv=True), l=True, fl=True)[0]]
	vertices = GN_GetBorderVertices(object, vtxPair)
	
	# Check that vertices are on a edge border
	if not len(vertices):
		print("// Error: Selected vertex not on a edge border!\n"),
		return
	
	# Query & disable symmetric modeling
	symEnabled = cmds.symmetricModelling(q=True, s=True)
	cmds.symmetricModelling(s=0)
	
	# Query units & set to centimeter
	units = cmds.currentUnit(q=True, l=True)
	cmds.currentUnit(l="cm")
	
	# Initialize Quad Fill Hole Boolean
	GN_QuadFillHole = False
	
	# Convert vertices to edges
	edges = cmds.ls(cmds.polyListComponentConversion(vertices, fv=True, te=True, internal=True), l=True, fl=True)
	faces = []
	
	# CASE 1: Edge border has 4 sides or less
	edgeCount = len(edges)
	if edgeCount <= 4:
		faces.append(GN_FillHole(object, edges))
		cmds.evalDeferred('print "// Result: GN_QuadFillHole\\n",')
	
	# CASE 2: Force alternate action
	elif forceAlternateAction:
		faces = GN_AlternateFillHole(object, edges, "")
	
	# CASE 3: Edge border doesn't have an even number of edges
	elif edgeCount % 2 == 1:
		msg = "Edge border doesn't have an even number of sides."
		faces = GN_AlternateFillHole(object, edges, msg)
		
		if alternateAction == 1:
			GN_RestoreSelection(selection, selectType, objects)
			cmds.symmetricModelling(s=symEnabled) # Put symmetric modeling in its previous state
			cmds.currentUnit(l=units) # Set previous units
			return
	
	# CASE 4: Quad Fill Hole
	else:
		# Get vertex pair
		traverse = edgeCount / 2
		
		if forceOffset or ((float(edgeCount) - 12) / 8) % 1 == 0:
			if offset == 2:
				traverse += 1
			elif offset == 3:
				traverse -= 1
				
		vtxPair.append(GN_VertexTraverse(vtxPair[0], vertices, traverse))
		
		# Create convex triangulated faces from edge border
		convex = cmds.ls(cmds.duplicate(object), l=True)[0]
		parent = cmds.listRelatives(object, f=True, p=True)
		
		if parent is not None:
			convex = cmds.ls(cmds.parent(convex, w=True), l=True)[0]
		
		vtxPair = GN_RelinkComponents(convex, vtxPair)
		convexEdges = GN_RelinkComponents(convex, edges)
		cmds.makeIdentity(convex, a=True, t=True, r=True, s=True, n=False, pn=True)
		cmds.polyMapDel(convex)
		faces = GN_CloseHole(convex, convexEdges)
		
		# Create UVs
		GN_CreateUVs(faces)
		uvs = cmds.ls(cmds.polyListComponentConversion(vtxPair, fv=True, tuv=True), l=True, fl=True)
		GN_OrientUvs(uvs[0], uvs[1])
		
		# Extract faces
		convex = GN_DeleteOtherFaces(convex, faces)
		
		# Get corner UVs
		cornerUvs = GN_GetCornerUvs(convex)
		
		# Check if corner UVs could be found properly
		if len(cornerUvs) != 4:
			msg = "Can't perform a grid fill on this shape."
			faces = GN_AlternateFillHole(object, edges, msg)
			if alternateAction == 1:
				GN_RestoreSelection(selection, selectType, objects)
				cmds.delete(convex) # Cleanup
				cmds.symmetricModelling(s=symEnabled) # Put symmetric modeling in its previous state
				cmds.currentUnit(l=units) # Set previous units
				return
		
		# Get in-between UVs
		uvsLeft = GN_InBetweenUvs(convex, cornerUvs, "left")
		uvsTop = GN_InBetweenUvs(convex, cornerUvs, "top")
		uvsRight = GN_InBetweenUvs(convex, cornerUvs, "right")
		uvsBottom = GN_InBetweenUvs(convex, cornerUvs, "bottom")
		
		# Check if object shape doesn't allow for a Quad Fill Hole
		if len(uvsLeft) != len(uvsRight) or len(uvsTop) != len(uvsBottom):
			msg = "Can't perform a grid fill. Try with another vertex."
			faces = GN_AlternateFillHole(object, edges, msg)
			if alternateAction == 1:
				GN_RestoreSelection(selection, selectType, objects)
				cmds.delete(convex) # Cleanup
				cmds.symmetricModelling(s=symEnabled) # Put symmetric modeling in its previous state
				cmds.currentUnit(l=units) # Set previous units
				return
		else:
			# Move corner UVs
			cmds.polyEditUV(cornerUvs[0], r=False, u=0, v=1)
			cmds.polyEditUV(cornerUvs[1], r=False, u=1, v=1)
			cmds.polyEditUV(cornerUvs[2], r=False, u=0, v=0)
			cmds.polyEditUV(cornerUvs[3], r=False, u=1, v=0)
			
			# Move in-between UVs
			GN_MoveUv(uvsLeft, "left")
			GN_MoveUv(uvsTop, "top")
			GN_MoveUv(uvsRight, "right")
			GN_MoveUv(uvsBottom, "bottom")
			
			# Reposition the in-between UVs so that they have an equal distance between each other
			GN_AvgUvDistance(uvsLeft, "v", 0)
			GN_AvgUvDistance(uvsTop, "u", 1)
			GN_AvgUvDistance(uvsRight, "v", 1)
			GN_AvgUvDistance(uvsBottom, "u", 0)
			
			# Create plane with matching subidivisions
			divisionY = len(uvsLeft) - 1
			divisionX = len(uvsTop) - 1
			plane = cmds.ls(cmds.polyPlane(ch=False, sx=divisionX, sy=divisionY, cuv=2), l=True)[0]
			
			# Left to Right (uvsBottom) | Bottom to Top (uvsLeft)
			count = 0
			for v in uvsLeft:
				vValue = cmds.polyEditUV(v, q=True)[1]
				
				for u in uvsBottom:
					uValue = cmds.polyEditUV(u, q=True)[0]
					uvValue = [uValue, vValue]
					
					pos = GN_GetPointAtUV(convex, uvValue)
					vertex = plane + ".vtx[" + str(count) + "]"
					
					cmds.xform(vertex, a=True, ws=True, t=[pos[0], pos[1], pos[2]])
					
					count += 1
			
			# Apply the material from the object to the plane
			sg = cmds.listConnections(shape, t="shadingEngine")[0]
			cmds.sets(plane, e=True, fe=sg)
			
			# Combine & merge plane with original mesh and get new faces
			planeFaceCount = cmds.polyEvaluate(plane, f=True)
			GN_Combine(object, plane)
			totalFaceCount = cmds.polyEvaluate(object, f=True)
			faceStart = str(totalFaceCount - planeFaceCount)
			faceEnd = str(totalFaceCount - 1)
			faces = [object + ".f["+ faceStart + ":" + faceEnd + "]"]
			
			# Set Quad Fill Hole Boolean to True
			GN_QuadFillHole = True
			
			# Print result
			cmds.evalDeferred('print "// Result: GN_QuadFillHole\\n",')
			
		# Cleanup
		cmds.delete(convex)
	
	# Add inset
	if inset:
		insetMultiplier = (GN_EdgeLength(edges) / 10) * insetSlider
		cmds.polyExtrudeFacet(faces, ch=False, off=insetMultiplier)
	
	# Average internal vertices
	if GN_QuadFillHole:
		vertices = cmds.polyListComponentConversion(faces, ff=True, tv=True, internal=True)
		
		if len(vertices):
			cmds.selectMode(co=True), cmds.selectType(pv=True)
			cmds.select(vertices, r=True)
			for i in xrange(20):
				cmds.polyAverageVertex(vertices, ch=False)
			
			cmds.select(object, r=True)
	
	# Create UVs
	if inset:
		vertices = cmds.polyListComponentConversion(faces, ff=True, tv=True)
		faces = cmds.polyListComponentConversion(vertices, fv=True, tf=True)
	
	GN_CreateUVs(faces)
	
	# Put symmetric modeling in its previous state
	cmds.symmetricModelling(s=symEnabled)
	
	# Set previous units
	cmds.currentUnit(l=units)
	
	# Select object
	cmds.selectMode(o=True)
	cmds.select(object, r=True)
	
	# Attach undo script job
	if cmds.optionVar(ex="GN_QuadFillHoleUndo") == 0 or not cmds.scriptJob(ex=cmds.optionVar(q="GN_QuadFillHoleUndo")):
		GN_QuadFillHoleUndo = cmds.scriptJob(e=["Undo", "GN_QuadFillHole.GN_QuadFillHoleUndo()"])
		cmds.optionVar(iv=("GN_QuadFillHoleUndo", GN_QuadFillHoleUndo))
	
	# Attach redo script job
	if cmds.optionVar(ex="GN_QuadFillHoleRedo") == 0 or not cmds.scriptJob(ex=cmds.optionVar(q="GN_QuadFillHoleRedo")):
		GN_QuadFillHoleRedo = cmds.scriptJob(e=["Redo", "GN_QuadFillHole.GN_QuadFillHoleRedo()"])
		cmds.optionVar(iv=("GN_QuadFillHoleRedo", GN_QuadFillHoleRedo))
	
	# Close undo chunk
	cmds.undoInfo(cck=True)

# Create a new partial class to print the correct command name when undoing
class rpartial(partial):
	def __repr__(self):
		return "GN_QuadFillHole"

# Create window
class QFH_Window(object):
	def __init__(self, optionBox=True, *args):
		# Create default variables
		self.pfx = "QFH_"
		self.windowName = self.pfx + "Window"
		self.windowSizeMenuItem = self.pfx + "SaveWindowSize_menuItem"

		self.windowTitle = "Quad Fill Hole"
		self.runLabel = "Quad Fill Hole"
		self.windowSize = [546, 350]

		# Create option variables
		self.saveWindowSize = self.pfx + "SaveWindowSize"
		self.alternateAction = self.pfx + "AlternateAction"
		self.forceAlternateAction = self.pfx + "ForceAlternateAction"
		self.inset = self.pfx + "Inset"
		self.insetSlider = self.pfx + "InsetSlider"
		self.offset = self.pfx + "Offset"
		self.forceOffset = self.pfx + "ForceOffset"

		# Create window or run command
		if optionBox == True:
			self.createWindow()
		else:
			self.runCmd(closeWindow=False)
		
	# Help command
	def helpCmd(self, *args):
		return
	
	# Run command
	def runCmd(self, closeWindow):
		self.defaultSettings(reset=False)
		GN_QuadFillHole()
		if closeWindow is True:
			self.closeWindow()
	
	# Close window
	def closeWindow(self):
		if cmds.window(self.windowName, ex=True) is True:
			cmds.deleteUI(self.windowName, wnd=True)
			
	# Window size
	def resizeWindow(self):
		if cmds.optionVar(q=self.saveWindowSize) == 0:
			cmds.window(self.windowName, e=True, wh=self.windowSize)
	
	# Default settings
	def defaultSettings(self, reset):
		if cmds.optionVar(ex=self.saveWindowSize) == 0:
			cmds.optionVar(iv=(self.saveWindowSize, 1))
		if reset == True or cmds.optionVar(ex=self.alternateAction) == 0:
			cmds.optionVar(iv=(self.alternateAction, 1))
		if reset == True or cmds.optionVar(ex=self.forceAlternateAction) == 0:
			cmds.optionVar(iv=(self.forceAlternateAction, 0))
		if reset == True or cmds.optionVar(ex=self.inset) == 0:
			cmds.optionVar(iv=(self.inset, 0))
		if reset == True or cmds.optionVar(ex=self.insetSlider) == 0:
			cmds.optionVar(fv=(self.insetSlider, 0.25))
		if reset == True or cmds.optionVar(ex=self.offset) == 0:
			cmds.optionVar(iv=(self.offset, 1))
		if reset == True or cmds.optionVar(ex=self.forceOffset) == 0:
			cmds.optionVar(iv=(self.forceOffset, 0))
	
	# Reset settings
	def resetSettings(self, *args):
		self.defaultSettings(reset=True)
		self.windowSetup()
	
	# Save settings
	def saveSettings(self, *args):
		cmds.optionVar(iv=(self.saveWindowSize, cmds.menuItem(self.windowSizeMenuItem, q=True, cb=True)))
		alternateAction = cmds.radioButtonGrp(self.alternateAction, q=True, sl=True)
		cmds.optionVar(iv=(self.alternateAction, alternateAction))
		forceAlternateAction = cmds.checkBoxGrp(self.forceAlternateAction, q=True, v1=True)
		cmds.optionVar(iv=(self.forceAlternateAction, forceAlternateAction))
		inset = cmds.checkBoxGrp(self.inset, q=True, v1=True)
		cmds.optionVar(iv=(self.inset, inset))
		cmds.optionVar(fv=(self.insetSlider, cmds.floatSliderGrp(self.insetSlider, q=True, v=True)))
		offset = cmds.radioButtonGrp(self.offset, q=True, sl=True)
		cmds.optionVar(iv=(self.offset, offset))
		cmds.optionVar(iv=(self.forceOffset, cmds.checkBoxGrp(self.forceOffset, q=True, v1=True)))
		
		if not inset:
			cmds.floatSliderGrp(self.insetSlider, e=True, en=False)
		else:
			cmds.floatSliderGrp(self.insetSlider, e=True, en=True)
		
		if alternateAction <= 1:
			cmds.checkBoxGrp(self.forceAlternateAction, e=True, en=False)
			forceAlternateAction = 0
		else:
			cmds.checkBoxGrp(self.forceAlternateAction, e=True, en=True)
			
		if forceAlternateAction:
			cmds.radioButtonGrp(self.offset, e=True, en=False)
			offset = 1
		else:
			cmds.radioButtonGrp(self.offset, e=True, en=True)
		
		if offset <= 1:
			cmds.checkBoxGrp(self.forceOffset, e=True, en=False)
		else:
			cmds.checkBoxGrp(self.forceOffset, e=True, en=True)
	
	# Setup window
	def windowSetup(self):
		cmds.menuItem(self.windowSizeMenuItem, e=True, cb=cmds.optionVar(q=self.saveWindowSize))
		cmds.radioButtonGrp(self.alternateAction, e=True, sl=cmds.optionVar(q=self.alternateAction))
		cmds.checkBoxGrp(self.forceAlternateAction, e=True, v1=cmds.optionVar(q=self.forceAlternateAction))
		cmds.checkBoxGrp(self.inset, e=True, v1=cmds.optionVar(q=self.inset))
		cmds.floatSliderGrp(self.insetSlider, e=True, v=cmds.optionVar(q=self.insetSlider))
		cmds.radioButtonGrp(self.offset, e=True, sl=cmds.optionVar(q=self.offset))
		cmds.checkBoxGrp(self.forceOffset, e=True, v1=cmds.optionVar(q=self.forceOffset))
		self.saveSettings()
	
	def initializeWindow(self):
		cmds.showWindow(self.windowName)
		self.resizeWindow()
		cmds.setFocus(self.windowName)
		
	# Create window
	def createWindow(self):
		# If window already opened, set focus on it
		if cmds.window(self.windowName, ex=True) is True:
			self.initializeWindow()
			return
		
		# Window
		cmds.window(self.windowName, t=self.windowTitle, mb=True, wh=self.windowSize)
		
		# Edit Menu
		cmds.menu(l="Edit")
		cmds.menuItem(l="Save Settings", c=partial(self.saveSettings), ecr=False)
		cmds.menuItem(l="Reset Settings", c=partial(self.resetSettings), ecr=False)
		cmds.menuItem(d=True)
		cmds.menuItem(self.windowSizeMenuItem, l="Save Window Size", c=partial(self.saveSettings), cb=False, ecr=False)
		cmds.setParent("..")
		
		# Help Menu
		cmds.menu(l="Help", helpMenu=True)
		cmds.menuItem(l="Help on "+self.windowTitle, i="help.png", c=partial(self.helpCmd), ecr=False)
		cmds.setParent("..")
		
		# Window Form (START)
		cmds.formLayout(self.pfx+"windowForm")
		
		# Tab Layout (START)
		cmds.tabLayout(self.pfx+"tabLayout", tv=False)
		cmds.formLayout(self.pfx+"tabForm")
		
		# Scroll Layout (START)
		cmds.scrollLayout(self.pfx+"scrollLayout", cr=True)
		cmds.formLayout(self.pfx+"scrollForm")
		
		# Description Frame (START)
		cmds.frameLayout(self.pfx+"Description_frameLayout", cll=True, l="Description", li=5, bgs=True, mh=4, mw=0)
		
		# Description Column (START)
		cmds.columnLayout(cat=("left", 20), adj=1)
		
		# Description
		cmds.text(l="Fills a hole with an even quad topology from a selected vertex.\nAlternate action will be used in situations where a Quad Fill Hole is not possible.", al="left")
		cmds.setParent("..")
		
		# Description Column (END)
		cmds.setParent("..")
		
		# Settings Frame (START)
		cmds.frameLayout(self.pfx+"Settings_frameLayout", cll=True, l="Settings", li=5, bgs=True, mh=4, mw=0)
		
		# Settings Column (START)
		cmds.columnLayout(cat=("left", 0), adj=1)
		
		# Alternate action
		cmds.radioButtonGrp(self.alternateAction, nrb=3, l="Alternate action:  ", la3=("None", "Fill Hole", "Close Hole"), cw=(1, 142), cc=partial(self.saveSettings))
		
		# Force Alternate Action
		cmds.checkBoxGrp(self.forceAlternateAction, l1="Force alternate action", cat=(1, "left", 143), cc=partial(self.saveSettings))
		
		# Separation
		cmds.rowLayout(h=3)
		cmds.setParent("..")
		cmds.rowLayout(h=2, bgc=[0.266, 0.266, 0.266])
		cmds.setParent("..")
		cmds.rowLayout(h=3)
		cmds.setParent("..")
		
		# Add inset
		cmds.checkBoxGrp(self.inset, l1="Add inset", cat=(1, "left", 143), cc=partial(self.saveSettings))
		
		# Inset slider
		cmds.floatSliderGrp(self.insetSlider, l="Inset: ", f=True, cat=(1, "left", 108), pre=4, min=0, max=1, fmn=0, fmx=1, cc=partial(self.saveSettings))
		
		# Offset
		cmds.radioButtonGrp(self.offset, nrb=3, l="Offset:  ", la3=("None", "+", "-"), cw=(1, 142), cc=partial(self.saveSettings))
		
		# Force offset
		cmds.checkBoxGrp(self.forceOffset, l1="Force offset", cat=(1, "left", 143), cc=partial(self.saveSettings))
		
		# Settings Column (END)
		cmds.setParent("..")
		
		# Settings Frame (END)
		cmds.setParent("..")
		
		# Scroll Layout (END)
		cmds.setParent("..")
		cmds.setParent("..")
		
		# Tab Layout (END)
		cmds.setParent("..")
		cmds.setParent("..")
		
		# Buttons
		cmds.formLayout(self.pfx+"Buttons_formLayout", nd=150)
		cmds.iconTextButton(self.pfx+"ApplyAndClose_button", st="textOnly", l=self.runLabel, c=rpartial(self.runCmd, closeWindow=True), fla=False, h=26, rpt=True)
		cmds.iconTextButton(self.pfx+"Apply_button", st="textOnly", l="Apply", c=rpartial(self.runCmd, closeWindow=False), fla=False, h=26, rpt=True)
		cmds.iconTextButton(self.pfx+"Close_button", st="textOnly", l="Close", c=partial(self.closeWindow), fla=False, h=26, rpt=True)
		cmds.setParent("..")
		
		# Window Form (END)
		cmds.setParent("..")
		
		# Window Form Layout
		cmds.formLayout(self.pfx+"windowForm", e=True,
			af=[(self.pfx+"tabLayout", "top", 0), (self.pfx+"tabLayout", "left", 0), (self.pfx+"tabLayout", "right", 0), (self.pfx+"tabLayout", "bottom", 36)])
		
		cmds.formLayout(self.pfx+"windowForm", e=True,
			ac=(self.pfx+"Buttons_formLayout", "top", 5, self.pfx+"tabLayout"),
			af=[(self.pfx+"Buttons_formLayout", "left", 5), (self.pfx+"Buttons_formLayout", "right", 5)],
			an=(self.pfx+"Buttons_formLayout", "bottom"))
		
		# Tabs Form Layout
		cmds.formLayout(self.pfx+"tabForm", e=True,
			af=[(self.pfx+"scrollLayout", "top", 2), (self.pfx+"scrollLayout", "left", 2), (self.pfx+"scrollLayout", "right", 2), (self.pfx+"scrollLayout", "bottom", 2)])
		
		# Scroll Form Layout
		cmds.formLayout(self.pfx+"scrollForm", e=True,
			af=[(self.pfx+"Settings_frameLayout", "top", 0), (self.pfx+"Settings_frameLayout", "left", 0), (self.pfx+"Settings_frameLayout", "right", 0)],
			an=(self.pfx+"Settings_frameLayout", "bottom"))
		
		cmds.formLayout(self.pfx+"scrollForm", e=True,
			af=[(self.pfx+"Description_frameLayout", "top", 0), (self.pfx+"Description_frameLayout", "left", 0), (self.pfx+"Description_frameLayout", "right", 0)],
			an=(self.pfx+"Description_frameLayout", "bottom"))
		
		cmds.formLayout(self.pfx+"scrollForm", e=True,
			ac=(self.pfx+"Settings_frameLayout", "top", 0, self.pfx+"Description_frameLayout"),
			af=[(self.pfx+"Settings_frameLayout", "left", 0), (self.pfx+"Settings_frameLayout", "right", 0)],
			an=(self.pfx+"Settings_frameLayout", "bottom"))
		
		# Buttons Form Layout
		cmds.formLayout(self.pfx+"Buttons_formLayout", e=True,
			af=[(self.pfx+"ApplyAndClose_button", "top", 0), (self.pfx+"ApplyAndClose_button", "left", 0)],
			ap=(self.pfx+"ApplyAndClose_button", "right", 2, 50),
			an=(self.pfx+"ApplyAndClose_button", "bottom"))
		
		cmds.formLayout(self.pfx+"Buttons_formLayout", e=True,
			af=(self.pfx+"Apply_button", "top", 0),
			ap=[(self.pfx+"Apply_button", "left", 2, 50), (self.pfx+"Apply_button", "right", 2, 100)],
			an=(self.pfx+"Apply_button", "bottom"))
			
		cmds.formLayout(self.pfx+"Buttons_formLayout", e=True,
			af=[(self.pfx+"Close_button", "top", 0), (self.pfx+"Close_button", "right", 0)],
			ap=(self.pfx+"Close_button", "left", 2, 100),
			an=(self.pfx+"Close_button", "bottom"))
		
		# Window Setup
		self.defaultSettings(reset=False)
		self.windowSetup()
		self.initializeWindow()