import pymel.core as pm
import pymel.util as pu

def main():
    scene_path = pu.getEnv("PIPE_PIPELINEPATH") + "/maya/lookdev_kit/scenes/lookdev.001.ma"
    pm.cmds.file(scene_path, ignoreVersion=1, type="mayaAscii", namespace="lookdev_001", r=1, gl=1, mergeNamespacesOnClash=False, options="v=0;")