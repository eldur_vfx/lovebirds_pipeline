INSTALATION:

- copy lookdev_kit folder from .zip to your MyDocuments Maya folder. It is usually here:

C:\Users\(User_Name)\Documents\maya


- run Maya


- open script editor and in MEL tab paste following line of code:

file -r -type "mayaAscii"  -ignoreVersion -gl -mergeNamespacesOnClash false -namespace "lookdev_001" -options "v=0;" "%MAYA_APP_DIR%/lookdev_kit/scenes/lookdev.001.ma";


This will reference Lookdev scene into your currently opened scene. You can always select that line of code in script editor and middle-click-drag it to your shelf to create a button, so you can call it when needed.


- Sky Radius attribute for Skydome is set to 0, HDRs are not properly displayed anyways.


- lookdev scene can be removed from your scene just by removing reference from reference editor. Just go to File > Reference Editor, select the lookdev scene and click Remove Reference.

All paths are relative to the My Documents Maya folder, do not change folder location or the scene will not work.



WORKFLOW:

Some general workflow would be to open an asset and than run a script so it references lookdev scene in your asset scene. When you finish lookdev proces, you can easily remove the Lookdev scene just by removing reference.

- 1-100 frame range is reserved for object rotation

- 101-200 frame range is reserved for skydome rotation

- parent constraint your asset to loc_object_rotation_01 so the asset will rotate for the first 100 frames

- skydome rotation offset slider is for corecting skydome rotation when needed or for rotating skydome for the first 100 frames

- there are 10 HDRs for skydome. You can change them by changing the "HDR version" attribute on the skydome shape node - numbers 0 to 9. Just select Skydome and go to the Channel Box, you should see HDR version attribute

- use camera that came with lookdev kit - macbeth and spheres are constrained to that camera



Created by Dusan Kovic - dusankovic.com

Thanks to Aleksandar Kocic (aleksandarkocic.com) and Arvid Schneider (arvidschneider.com) for some tips to make this more usable. Also thanks to all my friends who "beta tested" Lookdev kit.

HDR images taken from hdrihaven.com


RELEASE NOTES:

19.02.2019. Update 02:

- Added Focus Plane to control Arnold Depth of Field Focus Distance (aiFocusDistance)

- Depth of Field control attributes on the Focus plane

- added new HDR on HDR slot 01

- fixed problem with converting environment map to .tx. Thanks to Arnold Dev team! This was a major problem in previous version.


04.01.2019. Update 01:

- HDR selection slider - SUPER special thanks to Aleksandar Kocic for some coding magic that made the pivot snap to the HDR numbers. 

- removed albedo AOV that was left behind.

- I added some colors to the scene elements.

- scene is a bit more organised.

- loc_object_rotation_01 was left outside of the group because of parent constraint.

- global transform control for the entire Lookdev scene (move, rotate, scale).

KNOWN ISSUES:

01. HDR version slider knows to be a bit buggy, if the snapping stops, just deselect it and select it again.

02. I had to remove the .tx file and had to load original multi - layered .exr for Skydome. Due to the OIIO update in Arnold I couldn't load multi - layered .tx file. I didn't have any problems because of using .exr, I hope it will work for you. I asked Arnold devs about this and I hope solution is coming in the future. 

WARNING: Do not create .tx for lookdev_hdri_2k_01.exr, it will probably not work in newer versions of Arnold. 