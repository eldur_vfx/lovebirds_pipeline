# Script to change the NUKE_PATH to the default or to the StuPro Config depending of the current EnvVar

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
Write-Host $scriptPath
$NUKE_PATH=$scriptPath+"\nuke\Lovebirds.Inc"

$current_env = $([System.Environment]::GetEnvironmentVariable('NUKE_PATH','USER'))


if ($current_env -Match "Nextcloud"){
    [Environment]::SetEnvironmentVariable("NUKE_PATH","",'USER')
} else {
    [Environment]::SetEnvironmentVariable("NUKE_PATH","$NUKE_PATH",'USER')
}