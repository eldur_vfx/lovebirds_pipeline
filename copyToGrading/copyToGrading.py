import os
import re
import shutil

PIPE_SHOTPATH = os.getenv("PIPE_SHOTPATH")
PIPE_GRADINGPATH = os.getenv("PIPE_GRADINGPATH")


def copy_to_grading():
    shot_version_list = get_current_versions()
    print(shot_version_list)
    for shot in os.listdir(PIPE_SHOTPATH):
        print("--------")
        print("Current Shot: {}".format(shot))
        export_img_fin_path = os.path.join(PIPE_SHOTPATH, shot, 'comp', '_export', 'img-fin')
        if os.path.exists(export_img_fin_path):
            max_version = -1
            max_version_folder = ''
            for version_folder in os.listdir(export_img_fin_path):
                version_folder_parts = version_folder.split('_')
                if len(version_folder_parts) == 5:
                    version = re.findall("(_v)(\d{3})", version_folder)[0][1]
                    if int(max_version) < int(version):
                        max_version = version
                        max_version_folder = version_folder
            if shot_version_list.get(shot) is None or int(max_version) > shot_version_list.get(shot):
                max_version_folder_path = os.path.join(export_img_fin_path, max_version_folder)
                version_toGrading_path = os.path.join(PIPE_GRADINGPATH, shot, max_version_folder)
                shot_toGrading_path = os.path.join(PIPE_GRADINGPATH, shot)
                if os.path.exists(shot_toGrading_path):
                    for f in os.listdir(shot_toGrading_path):
                        old_version_path = os.path.join(PIPE_GRADINGPATH, shot, f)
                        print("Remove: {}".format(old_version_path))
                        shutil.rmtree(old_version_path)
                print("Start copying Version {} on shot {}".format(max_version, shot))
                destination = shutil.copytree(max_version_folder_path, version_toGrading_path)
                print("Copied to: {}".format(destination))
            else:
                print("Shot {} is up to date with version {}".format(shot, shot_version_list.get(shot)))
    print("--------")


def get_current_versions():
    shot_version_list = {}
    for shot in os.listdir(PIPE_GRADINGPATH):
        shot_folder = os.path.join(PIPE_GRADINGPATH, shot)
        for version_folder in os.listdir(os.path.join(PIPE_GRADINGPATH, shot_folder)):
            version_folder_parts = version_folder.split('_')
            if len(version_folder_parts) == 5:
                version = re.findall("(_v)(\d{3})", version_folder)[0][1]
                shot_version_list[shot] = int(version)
    return shot_version_list


def create_shot_folder():
    for shot in os.listdir(PIPE_SHOTPATH):
        shot_toGrading_path = os.path.join(PIPE_GRADINGPATH, shot)
        os.mkdir(shot_toGrading_path)


copy_to_grading()
